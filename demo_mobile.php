<?php 
if(isset($_GET['id']) && $_GET['id'] != ''){
	if($_GET['id']=='head'){
		$imagen_h='head_movil_h_320x100.png';
		$imagen_v='head_movil_v_320x100.png';
		$titulo='BANNER HEAD / INTERNOTICIA 320x100';
		$url_volver='index.php#tab-3';
		$ancho='800';
		$logos=true;
	}elseif($_GET['id']=='content'){
		$imagen_h='content_movil_h_300x250.png';
		$imagen_v='content_movil_v_300x250.png';
		$titulo='BANNER CONTENT 300x250';
		$url_volver='index.php#tab-3';
		$ancho='363';
		$logos=true;
	}elseif($_GET['id']=='itt'){
		$imagen_h='itt_movil_h_480x220.png';
		$imagen_v='itt_movil_v_320x370.png';
		$titulo='BANNER ITT 320x370 | 480x220';
		$url_volver='index.php#tab-3';
		$ancho='363';
		$logos=true;
	}
}else{
	header('Location:index.php');
}


include('header.php');
?>	

	<script type="text/javascript">
	    function showContent() {
			var div_display = document.getElementById('movil').style.display;
			if(div_display == "none"){
				document.getElementById("movil").style.display='block';
	            document.getElementById("movil2").style.display='none';
			}
			if(div_display == "block"){
				document.getElementById("movil").style.display='none';
	            document.getElementById("movil2").style.display='block';
			}
	    }
	</script>

    <div class="fila_contenido" style="margin-top:80px;">
    	<div class="contenido" style="font-weight:bold; text-align:center; font-size:21px;">
    		<?php echo $titulo?>
    	</div>
    </div>

    <div class="fila_contenido">
    	<div class="contenido" style="font-weight:bold; text-align:center; font-size:21px;">
    		<div id="cambiarVista">
				<input type="button" name="rotar" id="btn_rotar" onclick="javascript:showContent()" class="boton_rotar"/>
			</div>
    	</div>
    </div>

	<div class="fila_contenido">
    	<div class="contenido">
    	<div style="width:100%; height:auto; float:left;">
            <div id="movil" style="max-width:<?php echo $ancho?>px; margin:0 auto 0 auto; display:block;"><img src="images/<?php echo $imagen_v?>" width="100%"/></div>
    		<div id="movil2" style="max-width:734px; margin:0 auto 0 auto; display:none;"><img src="images/<?php echo $imagen_h?>" /></div>
    	</div>
		</div><!--cierra contenido-->
    </div><!--cierra fila contenido-->


    <?php if($logos == true){?>
    <div class="fila_contenido">
    	<div class="contenido">
    		<ul style="width:100%; height:auto; float:left; padding:0; text-align:center;">
    			<li class="item_lista_logos"><img src="images/logos/soychile.gif" width="100%" /></li>
    			<li class="item_lista_logos"><img src="images/logos/soyarica.gif" width="100%" /></li>
    			<li class="item_lista_logos"><img src="images/logos/soyiquique.gif" width="100%" /></li>
    			<li class="item_lista_logos"><img src="images/logos/soycalama.gif" width="100%" /></li>
    			<li class="item_lista_logos"><img src="images/logos/soyantofagasta.gif" width="100%" /></li>
    			<li class="item_lista_logos"><img src="images/logos/soycopiapo.gif" width="100%" /></li>
    			<li class="item_lista_logos"><img src="images/logos/soyvalparaiso.gif" width="100%" /></li>
    			<li class="item_lista_logos"><img src="images/logos/soyquillota.gif" width="100%" /></li>
    			<li class="item_lista_logos"><img src="images/logos/soysanantonio.gif" width="100%" /></li>
    			<li class="item_lista_logos"><img src="images/logos/soychillan.gif" width="100%" /></li>
    			<li class="item_lista_logos"><img src="images/logos/soysancarlos.gif" width="100%" /></li>
    			<li class="item_lista_logos"><img src="images/logos/soytome.gif" width="100%" /></li>
    			<li class="item_lista_logos"><img src="images/logos/soytalcahuano.gif" width="100%" /></li>
    			<li class="item_lista_logos"><img src="images/logos/soyconcepcion.gif" width="100%" /></li>
    			<li class="item_lista_logos"><img src="images/logos/soycoronel.gif" width="100%" /></li>
    			<li class="item_lista_logos"><img src="images/logos/soyarauco.gif" width="100%" /></li>
    			<li class="item_lista_logos"><img src="images/logos/soytemuco.gif" width="100%" /></li>
    			<li class="item_lista_logos"><img src="images/logos/soyvaldivia.gif" width="100%" /></li>
    			<li class="item_lista_logos"><img src="images/logos/soyosorno.gif" width="100%" /></li>
    			<li class="item_lista_logos"><img src="images/logos/soypuertomontt.gif" width="100%" /></li>
    			<li class="item_lista_logos"><img src="images/logos/soychiloe.gif" width="100%" /></li>
    		</ul>
    	</div>
    </div>
    <?php }?>


<?php include('footer.php');?>	