<?php include('header.php');?>

<?php $url_volver='index.php?tab=0';?>

<?php include('submenu.php');?>



<div class="contenedor_layout">
	<div class="contenido_autolocal">
		
        <div class="container_scroll">
        	<div class="columna_scroll">
        		<div class="sticky">
        			
                    <div id="menu">
                    	<div class="contenido_menu">
                        	<div class="titulo_menu">IR A LA MEDIDA</div>
                            <div class="links_menu">  
                            	<div class="item_links_menu">  
                                <span class="bull_menu">&bull;</span> <a href="#contenedor_header">HEAD 728x90</a>
                                </div>
                                
                                <div class="item_links_menu">
                                <span class="bull_menu">&bull;</span> <a href="#cont_300x250_superior">BOX SUPERIOR 300x250</a>
                                </div>
                                
                                <div class="item_links_menu">
                                <span class="bull_menu">&bull;</span> <a href="#cont_300x100_derecha_1">DERECHA 1 300x100</a>
                                </div>
                                
                                <div class="item_links_menu">
                                <span class="bull_menu">&bull;</span> <a href="#cont_650x90_central">CENTRAL 650x90</a>
                                </div>
                                
                                <div class="item_links_menu">
                                <span class="bull_menu">&bull;</span> <a href="#cont_300x250_medio_1">BOX MEDIO 1 300x250</a>
                                </div>
                                
                                <div class="item_links_menu">
                                <span class="bull_menu">&bull;</span> <a href="#cont_650x90_medio">MEDIO 650x90</a>
                                </div>
                                
                                <div class="item_links_menu">
                                <span class="bull_menu">&bull;</span> <a href="#cont_300x100_derecha_2">DERECHA 2 300x100</a>
                                </div>
                                
                                <div class="item_links_menu">
                                <span class="bull_menu">&bull;</span> <a href="#cont_300x250_medio_2">BOX MEDIO 2 300x250</a>
                                </div>
                                
                                <div class="item_links_menu">
                                <span class="bull_menu">&bull;</span> <a href="#cont_300x100_derecha_3">DERECHA 3 300x100</a>
                                </div>
                                
                                <div class="item_links_menu">
                                <span class="bull_menu">&bull;</span> <a href="#cont_650x90_inferior">INFERIOR 650x90</a>
                                </div>
                                
                                <div class="item_links_menu">
                                <span class="bull_menu">&bull;</span> <a href="#cont_300x250_inferior_1">INFERIOR 1 300x250</a>
                                </div>
                                
                                <div class="item_links_menu">
                                <span class="bull_menu">&bull;</span> <a href="#cont_300x100_derecha_4">DERECHA 4 300x100</a>
                                </div>
                                
                                <div class="item_links_menu">
                                <span class="bull_menu">&bull;</span> <a href="#cont_300x250_inferior_2">INFERIOR 2 300x250</a>
                                </div>
                            </div>
                        </div>
                    </div>
				
                </div>
	       	</div>
        </div>
		
        
        <div class="contenedor_img_layout">
			<div id="cont_728x90_head">
            	<div class="b_728x90_head"><img src="img/layout_728x90_head.jpg" /></div>
            </div>
            
            <div id="cont_300x250_superior">
            	<div class="b_300x250_superior"><img src="img/layout_300x250_box_superior.jpg" /></div>
            </div>
            
            <div id="cont_300x100_derecha_1">
            	<div class="b_300x100_derecha_1"><img src="img/layout_derecha_1_300x100.jpg" /></div>
            </div>
            
            <div id="cont_650x90_central">
            	<div class="b_650x90_central"><img src="img/layout_650x90.jpg" /></div>
            </div>
            
            <div id="cont_300x250_medio_1">
            	<div class="b_300x250_medio_1"><img src="img/layout_300x250_box_medio_1.jpg" /></div>
            </div>
            
            <div id="cont_650x90_medio">
            	<div class="b_650x90_medio"><img src="img/layout_650x90_medio.jpg" /></div>
            </div>
            
            <div id="cont_300x100_derecha_2">
            	<div class="b_300x100_derecha_2"><img src="img/layout_derecha_2_300x100.jpg" /></div>
            </div>
            
            <div id="cont_300x250_medio_2">
            	<div class="b_300x250_medio_2"><img src="img/layout_300x250_box_medio_2.jpg" /></div>
            </div>
            
            <div id="cont_300x100_derecha_3">
            	<div class="b_300x100_derecha_3"><img src="img/layout_derecha_3_300x100.jpg" /></div>
            </div>
            
            <div id="cont_650x90_inferior">
            	<div class="b_650x90_inferior"><img src="img/layout_650x90_inferior.jpg" /></div>
            </div>
            
            <div id="cont_300x250_inferior_1">
            	<div class="b_300x250_inferior_1"><img src="img/layout_300x250_inferior_1.jpg" /></div>
            </div>
            
            <div id="cont_300x100_derecha_4">
            	<div class="b_300x100_derecha_4"><img src="img/layout_derecha_4_300x100.jpg" /></div>
            </div>
            
            <div id="cont_300x250_inferior_2">
            	<div class="b_300x250_inferior_2"><img src="img/layout_300x250_inferior_2.jpg" /></div>
            </div>
        </div>
        
        <div id="fin"></div>
        
	</div>
</div>

</body>
</html>
