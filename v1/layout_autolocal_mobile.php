<?php include('header.php');?>

<?php $url_volver='index.php?tab=4';?>

<?php include('submenu.php');?>

<?php
	if(isset($_GET['id']) && $_GET['id'] == 'inicio'){
		$alto_img='591';
		$img_fondo='layout_autolocal_inicio_movil.jpg';
		$margen_top_banner='32';
		$margen_izq_banner='32';
	}elseif(isset($_GET['id']) && $_GET['id'] == 'home'){
		$alto_img='591';
		$img_fondo='layout_autolocal_home_movil.jpg';
		$margen_top_banner='32';
		$margen_izq_banner='32';
	}elseif(isset($_GET['id']) && $_GET['id'] == 'listados'){
		$alto_img='1385';
		$img_fondo='layout_autolocal_listados_movil.jpg';
		$margen_top_banner='35';
		$margen_izq_banner='33';
	}elseif(isset($_GET['id']) && $_GET['id'] == 'ficha'){
		$alto_img='1358';
		$img_fondo='layout_autolocal_ficha_movil.jpg';
		$margen_top_banner='35';
		$margen_izq_banner='33';
	}
?>

<div class="contenedor_layout">
	<div class="contenido_autolocal">
		
        <div class="container_scroll">
        	<div class="columna_scroll">
        		<div class="sticky">
        			
                    <div id="menu">
                    	<div class="contenido_menu">
                        	<div class="titulo_menu">IR A LA MEDIDA</div>
                            <div class="links_menu">  
                            	<div class="item_links_menu">  
                                <span class="bull_menu">&bull;</span> <a href="#cont_300x50_movil">HEAD 300x50</a>
                                </div>
                            </div>
                        </div>
                    </div>
				
                </div>
	       	</div>
        </div>
		
        
        <div class="contenedor_div_layout_mobile">
        <div class="contenedor_img_layout_mobile" style="height:<?php echo $alto_img?>px; background-image:url(img/<?php echo $img_fondo?>);">
			<div id="cont_300x50_movil" style="margin-top:<?php echo $margen_top_banner?>px; margin-left:<?php echo $margen_izq_banner?>px;">
            	<div class="b_300x50_movil"><img src="img/layout_300x50.jpg" /></div>
            </div>
        </div>
        </div>

        <div id="fin"></div>
        
	</div>
</div>

</body>
</html>
