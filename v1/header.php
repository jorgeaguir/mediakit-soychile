<?php 
	if(isset($_POST['boton'])){

		$nombre = $_POST['nombre'];
		$mail = $_POST['mail'];
		$telefono = $_POST['telefono'];
		$contacto = $_POST['contacto'];
		$comentarios = $_POST['comentarios'];

		$header = 'From: '.$nombre.' <'.$mail.'>' . " \r\n";
		$header .= "X-Mailer: PHP/" . phpversion() . " \r\n";
		$header .= "Mime-Version: 1.0 \r\n";
		$header .= "Content-Type: text/html";
		
		
		
		$mensaje = "<h4>Datos de contacto</h4><br>Han cotizado un auto a traves de Autolocal.com<br> Modelo: " . $modelo . " <br>";
		$mensaje .="Nombre:" . $nombre . " <br>";
		$mensaje .= "Telefono: " .$telefono . " <br>";
		$mensaje .= "E-Mail: " . $mail . " <br><br><br>";
		$mensaje .= "Comentarios: " . $comentarios . " <br>";
		
		$mensaje .= "Enviado el " . date('d/m/Y', time());
		
		if($contacto == 1){
			$para = 'mailparasoychile';
		}elseif($contacto == 2){
			$para = 'mailparaautolocal';
		}
		$asunto = 'Contacto desde Mediakit';
		
		mail($para, $asunto, utf8_decode($mensaje), $header);
		

		$mensaje_ok='Mensaje enviado correctamente.';
	}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="icon" type="image/png" href="favicon.png" />
<title>Mediakit | Medios Regionales</title>

<script src="js/jquery-1.9.1.min.js"></script>
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
<script src="SpryAssets/SpryTabbedPanels.js" type="text/javascript"></script>
<script src="js/scroll.js" type="text/javascript"></script>
<link href="SpryAssets/SpryTabbedPanels.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="css/estilos.css" />
<script type="text/javascript" src="SpryAssets/SpryURLUtils.js"></script>

<link href="owl-carousel/owl.carousel.css" rel="stylesheet">
<link href="owl-carousel/owl.theme.css" rel="stylesheet">

<script type="text/javascript">
var params = Spry.Utils.getLocationParamsAsObject();
</script>

</head>

<body style="background-color:#eaeaea; padding:0; margin:0; font-family: 'Open Sans', sans-serif;">

<div id="contenedor_header" class="contenedor_header">
	<div class="contenido_header">
    	<div class="logo_header"><a href="index.php"><img src="img/logo_mr.png" /></a></div>
    	<!--<div class="logo_autolocal_header"><a href="http://www.autolocal.cl" target="_blank"><img src="img/autolocal.png" /></a></div>
    	<div class="logo_soychile_header"><a href="http://www.soychile.cl" target="_blank"><img src="img/soychile.png" /></a></div>-->
    </div>
</div>

<script type="text/javascript">
	function mostrar(){
		document.getElementById('caja_alerta').style.display='table';
		document.getElementById('fondo_alerta').style.display='table';
	}
	function cerrar(){
		document.getElementById('caja_alerta').style.display='none';
		document.getElementById('fondo_alerta').style.display='none';
	}	
</script>

<div id="fondo_alerta" class="fondo_alertas" style="display:none;">
</div>

<table width="100%" id="caja_alerta" cellspacing="0" cellpadding="0" border="0" class="tabla_alertas" style="display:none;">
	<tr>
		<td>
			<?php include('formulario_contacto.php');?>
		</td>
	</tr>
</table>


<?php 
if(isset ($mensaje_ok)){?>
<script type="text/javascript">
$(document).ready(function() {
    setTimeout(function() {
        $(".alerta_total").fadeOut(500);
    },2000);
});
</script>
<div class="alerta_total">
<div id="fondo_alerta" class="fondo_alertas">
</div>

<table width="100%" id="caja_alerta" cellspacing="0" cellpadding="0" border="0" class="tabla_alertas">
	<tr>
		<td>
			<div class="caja_alertas">
				
				<div class="img_gracias_form"><img src="img/check_alerta.png" /></div>
				<div class="gracias_form"><?php echo $mensaje_ok?></div>
				
			</div>
		</td>
	</tr>
</table>
</div>
<?php }?>

