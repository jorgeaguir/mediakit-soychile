<?php include('header.php');?>

<?php include('submenu.php');?>
<div style="width:100%; height:auto; float:left;">
	<div class="contenido_autolocal">
		<div style="width:127px; height:auto; float:left; margin-right:20px;"><img src="img/soychile.png" /></div>
		<div style="width:127px; height:auto; float:left; margin-left:430px;"><img src="img/autolocal.png" /></div>
	</div>
</div>
<div class="contenedor_contenido">
    <div class="contenido_autolocal">
    	<!--<div class="titulo">FORMATOS DIGITALES</div>-->
    
      <div id="TabbedPanels1" class="TabbedPanels">
          <ul class="TabbedPanelsTabGroup">
          	<li class="TabbedPanelsTab" tabindex="0" >
            	Layout Soychile
			</li>
            <li class="TabbedPanelsTab" tabindex="0">
            	Red Soy Desktop
            </li>
            <li class="TabbedPanelsTab" tabindex="0">
            	Red Soy Mobile
            </li>
            <li class="TabbedPanelsTab" tabindex="0">
            	Papel digital
            </li>
            <li class="TabbedPanelsTab" tabindex="0" >
            	Layout Autolocal
			</li>
			<li class="TabbedPanelsTab" tabindex="0">
            	Autolocal Desktop
			</li>
			<li class="TabbedPanelsTab" tabindex="0">
            	Autolocal Mobile
			</li>
          </ul>
          <div class="TabbedPanelsContentGroup">

          	<div class="TabbedPanelsContent">
            	<ul class="ul_ficha">
            		<li class="item_ul_ficha">
            			<a href="layout_soychile.php"><img src="img/icono_desktop.jpg" /></a>
            			<div class="texto_item_ul_ficha"><a href="layout_soychile.php">Desktop</a></div>
            		</li>
            		<li class="item_ul_ficha">
            			<a href="layout_soychile_mobile.php"><img src="img/icono_movil.jpg" /></a>
            			<div class="texto_item_ul_ficha"><a href="layout_soychile_mobile.php">Mobile</a></div>
            		</li>
            	</ul>
            </div>

            <div class="TabbedPanelsContent">
            	<div class="contenido_ficha">

            		<div class="item_ficha">
                		<div class="titulo_ficha">ITT 800x600</div>
                        <div class="especificaciones">
                            <b>Dimensi&oacute;n:</b> 800x600 <span class="pipe_ficha">|</span> 
                            <b>Formato:</b> HTML5 / JPG / GIF<span class="pipe_ficha">|</span>
                            <b>Restricciones:</b> M&aacute;ximo 8 segundos de animaci&oacute;n. / Obligatorio bot&oacute;n de cierre. <span class="pipe_ficha">|</span>
                            <a href="demo.php?id=800x600_itt">VER DEMO</a>
						</div>
                        <div class="condiciones">Requiere reserva.<br />*Se debe enviar tracking de seguimiento.</div>
                	</div>

                    <div class="item_ficha">
                		<div class="titulo_ficha">HEAD 728x90</div>
                        <div class="especificaciones">
                            <b>Dimensi&oacute;n:</b> 728x90 <span class="pipe_ficha">|</span> 
                            <b>Formato:</b> HTML5 / GIF / JPG <span class="pipe_ficha">|</span> 
                            <b>Peso máximo:</b> 45kb (JPG y GIF) / 100kb (HTML5) <span class="pipe_ficha">|</span> 
                            <a href="demo.php?id=728x90_head">VER DEMO</a>
						</div>
                        <div class="condiciones">Requiere reserva.<br />*Se puede implementar por tag o material + tracking de seguimiento.</div>
                	</div>

                	<div class="item_ficha">
                		<div class="titulo_ficha">CARRUSEL</div>
                        <div class="especificaciones">
                            <b>Especificaciones:</b> Color de fondo que coincida con la gráfica entregada.<br />2 gráficas para la cabecera del carrusel (estas se alternan) con las siguientes medidas 
							<b>960x130px (Desktop)</b>.<br />
							Para los ítems del carrusel (m&iacute;nimo 5) las medidas son 220x110px.<br />
							Las im&aacute;genes miniatura deben venir con descripci&oacute;n del producto incluida.
 							<br /><br />
                            <b>Formato:</b> HTML5 / JPG / GIF <span class="pipe_ficha">|</span> 
                            <a href="demo.php?id=carrusel">VER DEMO</a> <span class="pipe_ficha">|</span> 
                            <a href="http://digital.mediosregionales.cl/carrusel/demo/index.php?dir=936" target="_blank">VER EJEMPLO</a>
						</div>
                        <div class="condiciones">Requiere reserva.<br />*Se debe enviar tracking de seguimiento.</div>
                	</div>

                	<div class="item_ficha">
                		<div class="titulo_ficha">BOX 300x250</div>
                        <div class="especificaciones">
                            <b>Dimensi&oacute;n:</b> 300x250 <span class="pipe_ficha">|</span> 
                            <b>Formato:</b> HTML5 / GIF / JPG <span class="pipe_ficha">|</span> 
                            <b>Peso máximo:</b> 45kb (JPG y GIF) / 100kb (HTML5) <span class="pipe_ficha">|</span>
                            <a href="demo.php?id=300x250_box_superior">VER DEMO</a>
						</div>
                        <div class="condiciones">Requiere reserva.<br />*Se puede implementar por tag o material + tracking de seguimiento.<br />*Este formato rota en posición central e inferior.</div>
                	</div>

                	<div class="item_ficha">
                		<div class="titulo_ficha">DERECHA 300x100</div>
                        <div class="especificaciones">
                            <b>Dimensi&oacute;n:</b> 300x100 <span class="pipe_ficha">|</span> 
                            <b>Formato:</b> HTML5 / GIF / JPG <span class="pipe_ficha">|</span> 
                            <b>Peso máximo:</b> 45kb (JPG y GIF) / 100kb (HTML5) <span class="pipe_ficha">|</span>
                            <a href="demo.php?id=300x100_derecha">VER DEMO</a>
						</div>
                        <div class="condiciones">Requiere reserva.<br />*Se puede implementar por tag o material + tracking de seguimiento.<br />*Este formato rota en posición central e inferior.</div>	
                	</div>

                	<div class="item_ficha">
                		<div class="titulo_ficha">VIDEO 300x250</div>
                        <div class="especificaciones">
                            <b>Dimensi&oacute;n:</b> 300x250 <span class="pipe_ficha">|</span> 
                            <b>Formato:</b> Se debe enviar URL de video en Youtube. <span class="pipe_ficha">|</span>
                            <a href="demo.php?id=300x250_video">VER DEMO</a>
						</div>
                        <div class="condiciones">Requiere reserva.<br />*Se debe enviar tracking de seguimiento.<br />*Tiene auto play sin sonido, el usuario lo puede activar (se replica el reproductor de Youtube).</div>
                	</div>

                	<div class="item_ficha">
                		<div class="titulo_ficha">INTERNOTICIA 650x90</div>
                        <div class="especificaciones">
                            <b>Dimensi&oacute;n:</b> 650x90 <span class="pipe_ficha">|</span> 
                            <b>Formato:</b> HTML5 / GIF / JPG <span class="pipe_ficha">|</span> 
                            <b>Peso máximo:</b> 45kb (JPG y GIF) / 100kb (HTML5) <span class="pipe_ficha">|</span>
                            <a href="demo.php?id=650x90_central">VER DEMO</a>
						</div>
                        <div class="condiciones">Requiere reserva.<br />*Se puede implementar por tag o material + tracking de seguimiento.</div>
                	</div>

                	<div class="item_ficha">
                		<div class="titulo_ficha">PREROLL</div>
                        <div class="especificaciones">
                            <b>Formato:</b> Se debe enviar link de video en Youtube. <span class="pipe_ficha">|</span>
                            <b>Caracter&iacute;stica:</b> Tiene la opci&oacute;n de saltar el anuncio a los 5 segundos. <span class="pipe_ficha">|</span>
                            <a href="demo.php?id=preroll">VER DEMO</a>
						</div>
                        <div class="condiciones">Requiere reserva.<br />*Se debe enviar tracking de seguimiento.</div>
                	</div>

                	<div class="item_ficha">
                		<div class="titulo_ficha">PUBLIPOST</div>
                        <div class="especificaciones">
                            <b>Formato:</b> Texto m&aacute;s 2 im&aacute;genes. <span class="pipe_ficha">|</span>
                            <b>Caracter&iacute;stica:</b> 3 d&iacute;as de destacado en el home (d&iacute;as a elecci&oacute;n).<span class="pipe_ficha">|</span>
                            <a href="demo.php?id=publipost">VER DEMO</a> <span class="pipe_ficha">|</span> 
                            <a href="http://www.soychile.cl/Santiago/Publirreportajes/2016/05/17/393736/ENAP-inicia-limpieza-y-anuncia-investigacion-interna-por-fuga-de-producto-en-Quintero.aspx" target="_blank">VER EJEMPLO</a>
						</div>
                        <div class="condiciones">Requiere reserva.</div>
                	</div>

                	<div class="item_ficha">
                		<div class="titulo_ficha">PUBLIPOST + BANNERS</div>
                        <div class="especificaciones">
                            <b>Dimensi&oacute;n:</b> 728x90 y 300x250. <span class="pipe_ficha">|</span>
                            <b>Formato:</b> Texto m&aacute;s 2 im&aacute;genes - Banners HTML5 / GIF / JPG <span class="pipe_ficha">|</span>
                            <b>Caracter&iacute;stica:</b> 3 d&iacute;as de destacado en el home (d&iacute;as a elecci&oacute;n).<span class="pipe_ficha">|</span>
                            <a href="demo.php?id=publipost_b">VER DEMO</a>
						</div>
                        <div class="condiciones">Requiere reserva.</div>
                	</div>

                	<div class="item_ficha">
                		<div class="titulo_ficha">EXPANDIBLE 910x90 a 910x250</div>
                        <div class="especificaciones">
                            <b>Dimensi&oacute;n:</b> 910x90 - 910x250 <span class="pipe_ficha">|</span> 
                            <b>Formato:</b> HTML5 / GIF / JPG <span class="pipe_ficha">|</span>
                            <b>Peso máximo:</b> 45kb (JPG y GIF) / 100kb (HTML5) <span class="pipe_ficha">|</span>
                            <a href="demo.php?id=910x90_expandible">VER DEMO</a>
						</div>
                        <div class="condiciones">Requiere reserva.<br />*Se puede implementar por tag o material + tracking de seguimiento.</div>
                	</div>

                	<div class="item_ficha">
                		<div class="titulo_ficha">BRANDING 300x560</div>
                        <div class="especificaciones">
                            <b>Dimensi&oacute;n:</b> 300x560 <span class="pipe_ficha">|</span> 
                            <b>Formato:</b> HTML5 / GIF / JPG <span class="pipe_ficha">|</span>
                            <b>Peso máximo:</b> 45kb (JPG y GIF) / 100kb (HTML5) <span class="pipe_ficha">|</span>
                            <a href="demo.php?id=300x560_branding">VER DEMO</a>
						</div>
                        <div class="condiciones">Requiere reserva.<br />*Se puede implementar por tag o material + tracking de seguimiento.</div>
                	</div>

                	<div class="item_ficha">
                		<div class="titulo_ficha">CONTENT 575x250</div>
                        <div class="especificaciones">
                            <b>Dimensi&oacute;n:</b> 575x250 <span class="pipe_ficha">|</span> 
                            <b>Formato:</b> HTML5 / GIF / JPG <span class="pipe_ficha">|</span>
                            <b>Peso máximo:</b> 45kb (JPG y GIF) / 100kb (HTML5) <span class="pipe_ficha">|</span>
                            <a href="demo.php?id=575x250_content">VER DEMO</a>
						</div>
                        <div class="condiciones">Requiere reserva.<br />*Se puede implementar por tag o material + tracking de seguimiento.</div>
                	</div>

                	<div class="item_ficha">
                		<div class="titulo_ficha">SCROLL 575x90</div>
                        <div class="especificaciones">
                            <b>Dimensi&oacute;n:</b> 575x90 <span class="pipe_ficha">|</span> 
                            <b>Formato:</b> HTML5 / GIF / JPG <span class="pipe_ficha">|</span>
                            <b>Peso máximo:</b> 45kb (JPG y GIF) / 100kb (HTML5) <span class="pipe_ficha">|</span>
                            <b>Caracter&iacute;stica:</b> Banner sigue al usuario por el sitio. <span class="pipe_ficha">|</span>
                            <a href="demo.php?id=575x90_scroll">VER DEMO</a>
						</div>
                        <div class="condiciones">Requiere reserva.<br />*Se puede implementar por tag o material + tracking de seguimiento.</div>
                	</div>
                    
                    
                </div>
            </div>


            <div class="TabbedPanelsContent">
            	<div class="contenido_ficha">
                	
                    <div class="item_ficha">
                		<div class="titulo_ficha">ITT 320x370</div>
                        <div class="especificaciones">
                            <b>Dimensi&oacute;n:</b> Horizontal 480x220 / Vertical 320x370 <span class="pipe_ficha">|</span> 
                            <b>Formato:</b> GIF / JPG <span class="pipe_ficha">|</span>
                            <b>Restricciones:</b> Max. 8 segundos. / Obligatorio bot&oacute;n de cierre. <span class="pipe_ficha">|</span>
                            <a href="demo_mobile.php?id=320x370_itt">VER DEMO</a>
						</div>
                        <div class="condiciones">Requiere reserva.<br />*Se debe enviar tracking de seguimiento.</div>
                	</div>

                	<div class="item_ficha">
                		<div class="titulo_ficha">HEAD / INTERNOTICIA 320x100</div>
                        <div class="especificaciones">
                            <b>Dimensi&oacute;n:</b> 320x100 <span class="pipe_ficha">|</span> 
                            <b>Formato:</b> HTML5 / GIF / JPG <span class="pipe_ficha">|</span>
                            <a href="demo_mobile.php?id=320x100_head">VER DEMO</a>
						</div>
                        <div class="condiciones">Requiere reserva.<br />*Se debe enviar tracking de seguimiento.<br />*Tambi&eacute;n se puede exhibir en posici&oacute;n central.</div>
                	</div>

                	<div class="item_ficha">
                		<div class="titulo_ficha">INTERNOTICIA 300x250</div>
                        <div class="especificaciones">
                            <b>Dimensi&oacute;n:</b> 300x250 <span class="pipe_ficha">|</span> 
                            <b>Formato:</b> HTML5 / GIF / JPG <span class="pipe_ficha">|</span>
                            <a href="demo_mobile.php?id=300x250">VER DEMO</a>
						</div>
                        <div class="condiciones">Requiere reserva.<br />*Se debe enviar tracking de seguimiento.<br />*Tambi&eacute;n se puede exhibir en posici&oacute;n central.</div>
                	</div>

                </div>
            </div>


            <div class="TabbedPanelsContent">
            	<div class="contenido_ficha">

            		<div class="item_ficha">
                		<div class="titulo_ficha">BIG BANNER</div>
                        <div class="especificaciones">
                            <b>Dimensi&oacute;n:</b> 2 IMGS DE 500x300 + VIDEO (Formato MP4) <span class="pipe_ficha">|</span> 
                            <b>Formato:</b> GIF (animado) / JPG / VIDEO MP4 <span class="pipe_ficha">|</span>
                            <b>Peso máximo:</b> 45kb (JPG y GIF) <span class="pipe_ficha">|</span>
                            <a href="demo.php?id=bigbanner">VER DEMO</a>
						</div>
                        <div class="condiciones">Requiere reserva.<br />*Se debe enviar tracking de seguimiento.<br />*Tambi&eacute;n se puede exhibir en posici&oacute;n central.</div>
                	</div>
                
                </div>
            </div>


            <div class="TabbedPanelsContent">
            	<ul class="ul_ficha_autolocal">
            		<li class="item_ul_ficha">
            			<img src="img/icono_desktop.jpg" />
            			<div class="texto_item_ul_ficha">Desktop</div>
            		</li>

            		<ul style="width:100%; margin:0 10px 0 10px; padding:10px 0 0 0; font-size:13px;">
            			<li style="width:120px; height:auto; list-style:none; border:1px solid #ccc; display:inline-block;">
            				<a href="layout_autolocal.php?id=home" style="color:#000;">HOME</a>
            			</li>
            			<li style="width:120px; height:auto; list-style:none; border:1px solid #ccc; display:inline-block;">
            				<a href="layout_autolocal.php?id=listados" style="color:#000;">LISTADOS</a>
            			</li>
            			<li style="width:120px; height:auto; list-style:none; border:1px solid #ccc; display:inline-block;">
            				<a href="layout_autolocal.php?id=ficha" style="color:#000;">FICHA</a>
            			</li>
            		</ul>

            	</ul>
            	<ul class="ul_ficha">
            		<li class="item_ul_ficha">
            			<img src="img/icono_movil.jpg" />
            			<div class="texto_item_ul_ficha">Mobile</div>
            		</li>

            		<ul style="width:100%; margin:0 10px 0 10px; padding:10px 0 0 0; font-size:13px;">
            			<li style="width:120px; height:auto; list-style:none; border:1px solid #ccc; display:inline-block;">
            				<a href="layout_autolocal_mobile.php?id=inicio" style="color:#000;">INICIO</a>
            			</li>
            			<li style="width:120px; height:auto; list-style:none; border:1px solid #ccc; display:inline-block;">
            				<a href="layout_autolocal_mobile.php?id=home" style="color:#000;">HOME</a>
            			</li>
            			<li style="width:120px; height:auto; list-style:none; border:1px solid #ccc; display:inline-block;">
            				<a href="layout_autolocal_mobile.php?id=listados" style="color:#000;">LISTADOS</a>
            			</li>
            			<li style="width:120px; height:auto; list-style:none; border:1px solid #ccc; display:inline-block;">
            				<a href="layout_autolocal_mobile.php?id=ficha" style="color:#000;">FICHA</a>
            			</li>
            		</ul>

            	</ul>
            </div>


            <div class="TabbedPanelsContent">
            	<div class="contenido_ficha">
                	
                    <div class="item_ficha">
                		<div class="titulo_ficha">400x400 LAYER</div>
                        <div class="especificaciones">
                            <b>Dimensi&oacute;n:</b> 400x400 <span class="pipe_ficha">|</span> 
                            <b>Formato:</b> GIF / JPG <span class="pipe_ficha">|</span>
                            <b>Peso máximo:</b> 45kb (JPG y GIF) <span class="pipe_ficha">|</span>
                            <a href="demo.php?id=400x400_home_autolocal">VER DEMO</a>
						</div>
                        <div class="condiciones">*Se puede implementar por tag o material + tracking de seguimiento.</div>
                	</div>
                    <div class="item_ficha">
                		<div class="titulo_ficha">728x90 HOME / LISTADOS / FICHA</div>
                        <div class="especificaciones">
                            <b>Dimensi&oacute;n:</b> 728x90 <span class="pipe_ficha">|</span> 
                            <b>Formato:</b> GIF / JPG <span class="pipe_ficha">|</span>
                            <b>Peso máximo:</b> 45kb (JPG y GIF) <span class="pipe_ficha">|</span>
                            <a href="demo.php?id=728x90_home_autolocal">VER DEMO</a>
						</div>
                        <div class="condiciones">*Se puede implementar por tag o material + tracking de seguimiento.</div>
                	</div>

                	<div class="item_ficha">
                		<div class="titulo_ficha">300x250 HOME / FICHA</div>
                        <div class="especificaciones">
                            <b>Dimensi&oacute;n:</b> 300x250 <span class="pipe_ficha">|</span> 
                            <b>Formato:</b> GIF / JPG <span class="pipe_ficha">|</span>
                            <b>Peso máximo:</b> 45kb (JPG y GIF) <span class="pipe_ficha">|</span>
                            <a href="demo.php?id=300x250_home_autolocal">VER DEMO</a>
						</div>
                        <div class="condiciones">*Se puede implementar por tag o material + tracking de seguimiento.</div>
                	</div>

                	<div class="item_ficha">
                		<div class="titulo_ficha">300x100 HOME</div>
                        <div class="especificaciones">
                            <b>Dimensi&oacute;n:</b> 300x100 <span class="pipe_ficha">|</span> 
                            <b>Formato:</b> GIF / JPG <span class="pipe_ficha">|</span>
                            <b>Peso máximo:</b> 45kb (JPG y GIF) <span class="pipe_ficha">|</span>
                            <a href="demo.php?id=300x100_home_autolocal">VER DEMO</a>
						</div>
                        <div class="condiciones">*Se puede implementar por tag o material + tracking de seguimiento.</div>
                	</div>

                	<div class="item_ficha">
                		<div class="titulo_ficha">620x100 HOME</div>
                        <div class="especificaciones">
                            <b>Dimensi&oacute;n:</b> 620x100 <span class="pipe_ficha">|</span> 
                            <b>Formato:</b> GIF / JPG <span class="pipe_ficha">|</span>
                            <b>Peso máximo:</b> 45kb (JPG y GIF) <span class="pipe_ficha">|</span>
                            <a href="demo.php?id=620x100_home_autolocal">VER DEMO</a>
						</div>
                        <div class="condiciones">*Se puede implementar por tag o material + tracking de seguimiento.</div>
                	</div>

                	<!--<div class="item_ficha">
                		<div class="titulo_ficha">LISTADOS 728x90</div>
                        <div class="especificaciones">
                            <b>Dimensi&oacute;n:</b> 728x90 <span class="pipe_ficha">|</span> 
                            <b>Formato:</b> GIF / JPG <span class="pipe_ficha">|</span>
                            <b>Peso máximo:</b> 45kb (JPG y GIF) <span class="pipe_ficha">|</span>
                            <a href="demo.php?id=728x90_listados_autolocal">VER DEMO</a>
						</div>
                        <div class="condiciones">*Se puede implementar por tag o material + tracking de seguimiento.</div>
                	</div>-->

                	<div class="item_ficha">
                		<div class="titulo_ficha">160x600 LISTADOS</div>
                        <div class="especificaciones">
                            <b>Dimensi&oacute;n:</b> 160x600 <span class="pipe_ficha">|</span> 
                            <b>Formato:</b> GIF / JPG <span class="pipe_ficha">|</span>
                            <b>Peso máximo:</b> 45kb (JPG y GIF) <span class="pipe_ficha">|</span>
                            <a href="demo.php?id=160x600_listados_autolocal">VER DEMO</a>
						</div>
                        <div class="condiciones">*Se puede implementar por tag o material + tracking de seguimiento.</div>
                	</div>

                	<!--<div class="item_ficha">
                		<div class="titulo_ficha">FICHA 728x90</div>
                        <div class="especificaciones">
                            <b>Dimensi&oacute;n:</b> 728x90 <span class="pipe_ficha">|</span> 
                            <b>Formato:</b> GIF / JPG <span class="pipe_ficha">|</span>
                            <b>Peso máximo:</b> 45kb (JPG y GIF) <span class="pipe_ficha">|</span>
                            <a href="demo.php?id=728x90_ficha_autolocal">VER DEMO</a>
						</div>
                        <div class="condiciones">*Se puede implementar por tag o material + tracking de seguimiento.</div>
                	</div>-->

                	<!--<div class="item_ficha">
                		<div class="titulo_ficha">FICHA 300x250</div>
                        <div class="especificaciones">
                            <b>Dimensi&oacute;n:</b> 300x250 <span class="pipe_ficha">|</span> 
                            <b>Formato:</b> GIF / JPG <span class="pipe_ficha">|</span>
                            <b>Peso máximo:</b> 45kb (JPG y GIF) <span class="pipe_ficha">|</span>
                            <b>Caracter&iacute;stica:</b> Banner sigue al usuario por el sitio. <span class="pipe_ficha">|</span>
                            <a href="demo.php?id=300x250_ficha_autolocal">VER DEMO</a>
						</div>
                        <div class="condiciones">*Se puede implementar por tag o material + tracking de seguimiento.</div>
                	</div>-->

                </div>
            </div>


            <div class="TabbedPanelsContent">
            	<div class="contenido_ficha">
                	
                	<!--<div class="item_ficha">
                		<div class="titulo_ficha">INICIO 300x50</div>
                        <div class="especificaciones">
                            <b>Dimensi&oacute;n:</b> 300x50 <span class="pipe_ficha">|</span> 
                            <b>Formato:</b> GIF / JPG <span class="pipe_ficha">|</span>
                            <a href="demo_mobile.php?id=300x50_autolocal_inicio">VER DEMO</a>
						</div>
                        <div class="condiciones">*Se puede implementar por tag o material + tracking de seguimiento.</div>
                	</div>-->

                	<div class="item_ficha">
                		<div class="titulo_ficha">300x50 INICIO / HOME / LISTADOS / FICHA</div>
                        <div class="especificaciones">
                            <b>Dimensi&oacute;n:</b> 300x50 <span class="pipe_ficha">|</span> 
                            <b>Formato:</b> GIF / JPG <span class="pipe_ficha">|</span>
                            <a href="demo_mobile.php?id=300x50_autolocal_home">VER DEMO</a>
						</div>
                        <div class="condiciones">*Se puede implementar por tag o material + tracking de seguimiento.</div>
                	</div>

                	<!--<div class="item_ficha">
                		<div class="titulo_ficha">LISTADOS 300x50</div>
                        <div class="especificaciones">
                            <b>Dimensi&oacute;n:</b> 300x50 <span class="pipe_ficha">|</span> 
                            <b>Formato:</b> GIF / JPG <span class="pipe_ficha">|</span>
                            <a href="demo_mobile.php?id=300x50_autolocal_listados">VER DEMO</a>
						</div>
                        <div class="condiciones">*Se puede implementar por tag o material + tracking de seguimiento.</div>
                	</div>

                	<div class="item_ficha">
                		<div class="titulo_ficha">FICHA 300x50</div>
                        <div class="especificaciones">
                            <b>Dimensi&oacute;n:</b> 300x50 <span class="pipe_ficha">|</span> 
                            <b>Formato:</b> GIF / JPG <span class="pipe_ficha">|</span>
                            <a href="demo_mobile.php?id=300x50_autolocal_ficha">VER DEMO</a>
						</div>
                        <div class="condiciones">*Se puede implementar por tag o material + tracking de seguimiento.</div>
                	</div>-->

                </div>
            </div>


            


            


          </div>
      </div>
      
    </div>
</div>
<script type="text/javascript">
var TabbedPanels1 = new Spry.Widget.TabbedPanels("TabbedPanels1");
var TabbedPanels1 = new Spry.Widget.TabbedPanels("TabbedPanels1", {defaultTab: params.tab ? params.tab : 0});
</script>
</body>
</html>
