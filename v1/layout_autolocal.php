<?php include('header.php');?>

<?php $url_volver='index.php?tab=4';?>

<?php include('submenu.php');?>

<?php
if(isset($_GET['id']) && $_GET['id'] == 'home'){
	$alto_img='1632';
	$img='layout_autolocal_home.jpg';
}elseif(isset($_GET['id']) && $_GET['id'] == 'listados'){
	$alto_img='3868';
	$img='layout_autolocal_listados.jpg';
}elseif(isset($_GET['id']) && $_GET['id'] == 'ficha'){
	$alto_img='1438';
	$img='layout_autolocal_ficha.jpg';
}
?>

<div class="contenedor_layout">
	<div class="contenido_autolocal">
		
        <div class="container_scroll">
        	<div class="columna_scroll">
        		<div class="sticky">
        			
                    <div id="menu">
                    	<div class="contenido_menu">
                        	<div class="titulo_menu">IR A LA MEDIDA</div>
                            <div class="links_menu">
                            	<?php if(isset($_GET['id']) && $_GET['id'] == 'home'){?>  
	                            	<div class="item_links_menu">  
	                                	<span class="bull_menu">&bull;</span> <a href="#contenedor_header">728x90 SUPERIOR</a>
	                                </div>
	                                <div class="item_links_menu">  
	                                	<span class="bull_menu">&bull;</span> <a href="#cont_300x250_superior_autolocal_home">300x250 SUPERIOR</a>
	                                </div>
	                                <div class="item_links_menu">  
	                                	<span class="bull_menu">&bull;</span> <a href="#cont_620x100_autolocal_home">620x100</a>
	                                </div>
	                                <div class="item_links_menu">  
	                                	<span class="bull_menu">&bull;</span> <a href="#cont_300x100_autolocal_home_1">300x100 SUPERIOR</a>
	                                </div>
	                                <div class="item_links_menu">  
	                                	<span class="bull_menu">&bull;</span> <a href="#cont_300x250_medio_autolocal_home">300x250 MEDIO</a>
	                                </div>
	                                <div class="item_links_menu">  
	                                	<span class="bull_menu">&bull;</span> <a href="#cont_300x100_autolocal_home_2">300x100 MEDIO</a>
	                                </div>
	                                <div class="item_links_menu">  
	                                	<span class="bull_menu">&bull;</span> <a href="#cont_300x100_autolocal_home_3">300x100 INFERIOR 1</a>
	                                </div>
	                                <div class="item_links_menu">  
	                                	<span class="bull_menu">&bull;</span> <a href="#cont_300x100_autolocal_home_4">300x100 INFERIOR 2</a>
	                                </div>
	                                <div class="item_links_menu">  
	                                	<span class="bull_menu">&bull;</span> <a href="#cont_728x90_inferior_autolocal_home">728x90 INFERIOR</a>
	                                </div>
                                <?php }elseif(isset($_GET['id']) && $_GET['id'] == 'listados'){?>
                                	<div class="item_links_menu">  
	                                	<span class="bull_menu">&bull;</span> <a href="#contenedor_header">728x90 SUPERIOR</a>
	                                </div>
	                                <div class="item_links_menu">  
	                                	<span class="bull_menu">&bull;</span> <a href="#cont_160x600_autolocal_listados">160x600</a>
	                                </div>
	                                <div class="item_links_menu">  
	                                	<span class="bull_menu">&bull;</span> <a href="#cont_728x90_inferior_autolocal_listados">728x90 INFERIOR</a>
	                                </div>
                                <?php }elseif(isset($_GET['id']) && $_GET['id'] == 'ficha'){?>
                                	<div class="item_links_menu">  
	                                	<span class="bull_menu">&bull;</span> <a href="#contenedor_header">728x90 SUPERIOR</a>
	                                </div>
	                                <div class="item_links_menu">  
	                                	<span class="bull_menu">&bull;</span> <a href="#cont_300x250_autolocal_ficha">300x250</a>
	                                </div>
	                                <div class="item_links_menu">  
	                                	<span class="bull_menu">&bull;</span> <a href="#cont_728x90_inferior_autolocal_ficha">728x90 INFERIOR</a>
	                                </div>
                                <?php }?>
                            </div>
                        </div>
                    </div>
				
                </div>
	       	</div>
        </div>
		
        
        <div class="contenedor_img_layout" style="height:<?php echo $alto_img?>px; background-image:url(img/<?php echo $img?>);" >
        	<?php if(isset($_GET['id']) && $_GET['id'] == 'home'){?>
				<div id="cont_728x90_head_autolocal_home">
	            	<div class="b_728x90_head_autolocal_home"><img src="img/layout_728x90_superior.jpg" /></div>
	            </div>

	            <div id="cont_300x250_superior_autolocal_home">
	            	<div class="b_300x250_superior_autolocal_home"><img src="img/layout_300x250_superior.jpg" /></div>
	            </div>

	            <div id="cont_620x100_autolocal_home">
	            	<div class="b_620x100_autolocal_home"><img src="img/layout_620x100.jpg" /></div>
	            </div>

	            <div id="cont_300x100_autolocal_home_1">
	            	<div class="b_300x100_autolocal_home_1"><img src="img/layout_300x100_superior.jpg" /></div>
	            </div>

	            <div id="cont_300x250_medio_autolocal_home">
	            	<div class="b_300x250_medio_autolocal_home"><img src="img/layout_300x250_medio.jpg" /></div>
	            </div>

	            <div id="cont_300x100_autolocal_home_2">
	            	<div class="b_300x100_autolocal_home_2"><img src="img/layout_300x100_medio.jpg" /></div>
	            </div>

	            <div id="cont_300x100_autolocal_home_3">
	            	<div class="b_300x100_autolocal_home_3"><img src="img/layout_300x100_inferior_1.jpg" /></div>
	            </div>

	            <div id="cont_300x100_autolocal_home_4">
	            	<div class="b_300x100_autolocal_home_4"><img src="img/layout_300x100_inferior_2.jpg" /></div>
	            </div>

	            <div id="cont_728x90_inferior_autolocal_home">
	            	<div class="b_728x90_inferior_autolocal_home"><img src="img/layout_728x90_inferior.jpg" /></div>
	            </div>
	        <?php }elseif(isset($_GET['id']) && $_GET['id'] == 'listados'){?>
	        	<div id="cont_728x90_head_autolocal_listados">
	            	<div class="b_728x90_head_autolocal_listados"><img src="img/layout_728x90_superior.jpg" /></div>
	            </div>
	            <div id="cont_160x600_autolocal_listados">
	            	<div class="b_160x600_autolocal_listados"><img src="img/layout_160x600.jpg" /></div>
	            </div>
	            <div id="cont_728x90_inferior_autolocal_listados">
	            	<div class="b_728x90_inferior_autolocal_listados"><img src="img/layout_728x90_inferior.jpg" /></div>
	            </div>
	        <?php }elseif(isset($_GET['id']) && $_GET['id'] == 'ficha'){?>
	        	<div id="cont_728x90_head_autolocal_ficha">
	            	<div class="b_728x90_head_autolocal_ficha"><img src="img/layout_728x90_superior.jpg" /></div>
	            </div>
	            <div id="cont_300x250_autolocal_ficha">
	            	<div class="b_300x250_autolocal_ficha"><img src="img/layout_300x250.jpg" /></div>
	            </div>
	            <div id="cont_728x90_inferior_autolocal_ficha">
	            	<div class="b_728x90_inferior_autolocal_ficha"><img src="img/layout_728x90_inferior.jpg" /></div>
	            </div>
	        <?php }?>
        </div>
        
        <div id="fin"></div>
        
	</div>
</div>

</body>
</html>
