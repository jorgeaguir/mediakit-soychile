<?php include('header.php');?>

<?php
if(isset($_GET['id']) && ($_GET['id'] == '320x100_head')){
	$ancho_banner_h='320';
	$alto_banner_h='100';
	$ancho_banner_v='320';
	$alto_banner_v='100';
	$margen_izq_v='22';
	$margen_sup_v='253';
	$margen_izq_h='135';
	$margen_sup_h='-118';
	$pieza_v='<img src="img/demo_320x100_mobile.jpg" />';
	$pieza_h='<img src="img/demo_320x100_mobile.jpg" />';
	$imagen='img/320x100_movil.jpg';
	$imagen_h='img/320x100_movil_h.jpg';
	$logos=true;
	$url_volver='index.php?tab=2';
}elseif(isset($_GET['id']) && ($_GET['id'] == '320x370_itt')){
	$ancho_banner_h='320';
	$alto_banner_h='370';
	$ancho_banner_v='480';
	$alto_banner_v='220';
	$margen_izq_v='22';
	$margen_sup_v='180';
	$margen_izq_h='50';
	$margen_sup_h='-270';
	$pieza_v='<img src="img/demo_320x370_mobile.jpg" />';
	$pieza_h='<img src="img/demo_480x220_mobile.jpg" />';
	$imagen='img/320x100_movil.jpg';
	$imagen_h='img/320x480_movil_h.jpg';
	$logos=true;
	$url_volver='index.php?tab=2';
}elseif(isset($_GET['id']) && ($_GET['id'] == '300x50_autolocal_inicio')){
	$ancho_banner_h='300';
	$alto_banner_h='50';
	$ancho_banner_v='300';
	$alto_banner_v='50';
	$margen_izq_v='33';
	$margen_sup_v='183';
	$margen_izq_h='143';
	$margen_sup_h='-196';
	$pieza_v='<img src="img/demo_300x50_autolocal_movil.jpg" />';
	$pieza_h='<img src="img/demo_300x50_autolocal_movil.jpg" />';
	$imagen='img/300x50_autolocal_inicio_movil_v.jpg';
	$imagen_h='img/300x50_autolocal_inicio_movil_h.jpg';
	$logos=false;
	$url_volver='index.php?tab=6';
}elseif(isset($_GET['id']) && ($_GET['id'] == '300x50_autolocal_home')){
	$ancho_banner_h='300';
	$alto_banner_h='50';
	$ancho_banner_v='300';
	$alto_banner_v='50';
	$margen_izq_v='33';
	$margen_sup_v='183';
	$margen_izq_h='143';
	$margen_sup_h='-200';
	$pieza_v='<img src="img/demo_300x50_autolocal_movil.jpg" />';
	$pieza_h='<img src="img/demo_300x50_autolocal_movil.jpg" />';
	$imagen='img/300x50_autolocal_home_movil_v.jpg';
	$imagen_h='img/300x50_autolocal_home_movil_h.jpg';
	$logos=false;
	$url_volver='index.php?tab=6';
}elseif(isset($_GET['id']) && ($_GET['id'] == '300x50_autolocal_listados')){
	$ancho_banner_h='300';
	$alto_banner_h='50';
	$ancho_banner_v='300';
	$alto_banner_v='50';
	$margen_izq_v='33';
	$margen_sup_v='183';
	$margen_izq_h='143';
	$margen_sup_h='-200';
	$pieza_v='<img src="img/demo_300x50_autolocal_movil.jpg" />';
	$pieza_h='<img src="img/demo_300x50_autolocal_movil.jpg" />';
	$imagen='img/300x50_autolocal_listados_movil_v.jpg';
	$imagen_h='img/300x50_autolocal_listados_movil_h.jpg';
	$logos=false;
	$url_volver='index.php?tab=6';
}elseif(isset($_GET['id']) && ($_GET['id'] == '300x50_autolocal_ficha')){
	$ancho_banner_h='300';
	$alto_banner_h='50';
	$ancho_banner_v='300';
	$alto_banner_v='50';
	$margen_izq_v='33';
	$margen_sup_v='183';
	$margen_izq_h='143';
	$margen_sup_h='-200';
	$pieza_v='<img src="img/demo_300x50_autolocal_movil.jpg" />';
	$pieza_h='<img src="img/demo_300x50_autolocal_movil.jpg" />';
	$imagen='img/300x50_autolocal_ficha_movil_v.jpg';
	$imagen_h='img/300x50_autolocal_ficha_movil_h.jpg';
	$logos=false;
	$url_volver='index.php?tab=6';
}elseif(isset($_GET['id']) && ($_GET['id'] == '300x250')){
	$ancho_banner_h='300';
	$alto_banner_h='250';
	$ancho_banner_v='300';
	$alto_banner_v='250';
	$margen_izq_v='33';
	$margen_sup_v='263';
	$margen_izq_h='143';
	$margen_sup_h='-257';
	$pieza_v='<img src="img/demo_300x250_mobile.jpg" />';
	$pieza_h='<img src="img/demo_300x250_mobile.jpg" />';
	$imagen='img/300x250_movil_v.jpg';
	$imagen_h='img/300x250_movil_h.jpg';
	$logos=true;
	$url_volver='index.php?tab=2';
}
?>

<script type="text/javascript">
    function showContent() {
		var div_display = document.getElementById('content').style.display;
		if(div_display == "none"){
			document.getElementById("content").style.display='block';
            document.getElementById("content2").style.display='none';
		}
		if(div_display == "block"){
			document.getElementById("content").style.display='none';
            document.getElementById("content2").style.display='block';
		}
    }
</script>


<?php include('submenu.php');?>


<div id="cambiarVista">
	<input type="button" name="rotar" id="btn_rotar" onclick="javascript:showContent()" class="btn_rotar"/>
</div>

<div style="width:100%; height:auto; float:left; margin-top:57px;">
	<div class="contenido">
       	<div id="content" class="contenedor_celular_v" style="display:block;">
		<div class="contenedor_img_celular_v">
            <img src="<?php echo $imagen?>" />
        </div>

        <div class="contenedor_banner_m">
        	
            <div style="width:<?php echo $ancho_banner_v?>px; height:<?php echo $alto_banner_v?>px; margin-left:<?php echo $margen_izq_v?>px; margin-top:<?php echo $margen_sup_v?>px; position:absolute; ">
            	<?php echo $pieza_v?>
            </div>

       	</div>
    	</div>


    	<div id="content2" class="contenedor_celular_h" style="display:none;">
		<div class="contenedor_img_celular_h">
            <img src="<?php echo $imagen_h	?>" />
            <div class="contenedor_banner_h">
        	
	            <div style="width:<?php echo $ancho_banner_h?>px; height:<?php echo $alto_banner_h?>px; margin-left:<?php echo $margen_izq_h?>px; margin-top:<?php echo $margen_sup_h?>px; position:absolute; ">
	            	<?php echo $pieza_h?>
	            </div>

	       	</div>

        </div>
    	</div>  
    </div>
</div>

<?php if($logos == true){?>
	<div style="width:1003px; height:130px;margin-left:auto; margin-right:auto; z-index:1000;">
        <img src="img/logos_soy.png" />
    </div>
<?php }?>

</body>
</html>
