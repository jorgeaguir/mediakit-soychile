<?php include('header.php');?>

<?php $url_volver='index.php?tab=0';?>

<?php include('submenu.php');?>



<div class="contenedor_layout">
	<div class="contenido_autolocal">
		
        <div class="container_scroll">
        	<div class="columna_scroll">
        		<div class="sticky">
        			
                    <div id="menu">
                    	<div class="contenido_menu">
                        	<div class="titulo_menu">IR A LA MEDIDA</div>
                            <div class="links_menu">  
                            	<div class="item_links_menu">  
                                <span class="bull_menu">&bull;</span> <a href="#cont_320x100_movil">HEAD 320x100</a>
                                </div>
                                <div class="item_links_menu">  
                                <span class="bull_menu">&bull;</span> <a href="#cont_300x250_movil">CONTENT 300x250</a>
                                </div>
                            </div>
                        </div>
                    </div>
				
                </div>
	       	</div>
        </div>
		
        
        <div class="contenedor_div_layout_mobile">
        <div class="contenedor_img_layout_mobile">
			<div id="cont_320x100_movil">
            	<div class="b_320x100_movil"><img src="img/layout_320x100_mobile.jpg" /></div>
            </div>
            <div id="cont_300x250_movil">
            	<div class="b_300x250_movil"><img src="img/layout_300x250_mobile.jpg" /></div>
            </div>
            <div id="cont_300x250_movil_2">
            	<div class="b_300x250_movil"><img src="img/layout_300x250_mobile.jpg" /></div>
            </div>
            <div id="cont_320x100_movil_2">
            	<div class="b_320x100_movil"><img src="img/layout_320x100_mobile.jpg" /></div>
            </div>
            <div id="cont_320x100_movil_3">
            	<div class="b_320x100_movil"><img src="img/layout_320x100_mobile.jpg" /></div>
            </div>
            <div id="cont_300x250_movil_3">
            	<div class="b_300x250_movil"><img src="img/layout_300x250_mobile.jpg" /></div>
            </div>
        </div>
        </div>

        <div id="fin"></div>
        
	</div>
</div>

</body>
</html>
