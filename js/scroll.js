
	// Funcion para que un div siga el scroll
	function fixedScroll(ele, bottom) {
		margin_top_default = 0;	// tamano del item que esta sobre del que deseo que siga el scroll
		margin_top_from_page = 70;	// es el margen que separa el item que sigue el scroll del borde superior del navegador
		eleOffset = $(ele).offset().top + margin_top_default + margin_top_from_page; // posicion top actual del elemento
		eleHeight = $('.sticky').height(); // altura del contenido del elemento
		if($(window).scrollTop() + margin_top_default + margin_top_from_page >= $(bottom).offset().top-eleHeight) {
			$('.sticky').attr('class','sticky absolute');
			pos_bottom = $(bottom).offset().top - eleHeight - eleOffset;
			$('.sticky').attr('style','top:' + pos_bottom + "px;");
		}else if ($(window).scrollTop() >= eleOffset - margin_top_default) {
			$('.sticky').attr('class','sticky fixed');
			$('.sticky').attr('style','top:'+ margin_top_from_page + "px;");
		}else {
			$('.sticky').attr('class','sticky');
			$('.sticky').attr('style','');
		}
	}
	
	$(window).scroll(function(){
		fixedScroll($('.columna_scroll'),$('#fin'));
	});




