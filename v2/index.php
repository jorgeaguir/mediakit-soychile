<?php include('header.php');?>	

	<div class="fila_contenido">
    	<div class="contenido">

            <!--Horizontal Tab-->
            <div id="horizontalTab">
                <ul>
                    <li><a href="#tab-1">Layout Soychile</a></li>
                    <li><a href="#tab-2">Red Soy Desktop</a></li>
                    <li><a href="#tab-3">Red Soy Mobile</a></li>
                    <li><a href="#tab-4">Papel Digital</a></li>
                </ul>
        
                <div id="tab-1">
                    <ul style="width:100%; height:auto; padding:0; margin:0; text-align:center;">
                    	<li style="width:180px; height:auto; list-style:none; display:inline-block; border:1px solid #666; margin:20px;">
                    		<div style="width:180px; height:180px;"><a href="layout.php?id=desktop"><img src="images/icono_desktop.jpg" /></a></div>
                    		<div style="width:180px; height:auto; font-size:14px;"><a href="layout.php?id=desktop">Desktop</a></div>
                    	</li>

                    	<li style="width:180px; height:auto; list-style:none; display:inline-block; border:1px solid #666; margin:20px;">
                    		<div style="width:180px; height:180px;"><a href="layout.php?id=mobile"><img src="images/icono_movil.jpg" /></a></div>
                    		<div style="width:180px; height:auto; font-size:14px;"><a href="layout.php?id=mobile">Mobile</a></div>
                    	</li>
                    </ul>
                </div>
                <div id="tab-2">
                    <div class="contenedor_elemento">
                    	<div class="titulo_elemento">BANNER HEAD</div>
                        <div class="contenedor_info_elemento">
                        	<div class="info_elemento"><b>Dimensi&oacute;n:</b> 728x125</div>
                            <div class="pipe_elemento">|</div>
                            <div class="info_elemento"><b>Formato:</b> HTML5 / JPG / GIF</div>
                            <div class="pipe_elemento">|</div>
                            <div class="info_elemento"><b>Peso m&aacute;ximo:</b> 45kb (JPG y GIF) / 100kb (HTML5)</div>
                            <div class="pipe_elemento">|</div>
                            <div class="ver_demo_elemento"><a href="demo.php?id=head">VER DEMO</a></div>
                            <br /><span style="font-style:italic; color:#666; font-size:12px;">Requiere reserva.</span>
                            <br /><span style="font-style:italic; color:#666; font-size:12px;">Se puede implementar por tag o material + tracking de seguimiento.</span>
                        </div>
                    </div>
                    
                    <div class="contenedor_elemento">
                    	<div class="titulo_elemento">BANNER HEAD EXPANDIBLE</div>
                        <div class="contenedor_info_elemento">
                        	<div class="info_elemento"><b>Dimensi&oacute;n:</b> 728x125 - 728x300</div>
                            <div class="pipe_elemento">|</div>
                            <div class="info_elemento"><b>Observaci&oacute;n:</b> Se deben enviar 2 gr&aacute;ficas, una para el banner expandido y otra para el banner cerrado.</div>
                            <div class="pipe_elemento">|</div>
                            <div class="info_elemento"><b>Formato:</b> HTML5 / JPG / GIF</div>
                            <div class="pipe_elemento">|</div>
                            <div class="info_elemento"><b>Peso m&aacute;ximo:</b> 45kb (JPG y GIF) / 100kb (HTML5)</div>
                            <div class="pipe_elemento">|</div>
                            <div class="ver_demo_elemento"><a href="demo.php?id=head_expandible">VER DEMO</a></div>
                            <br /><span style="font-style:italic; color:#666; font-size:12px;">Requiere reserva.</span>
                            <br /><span style="font-style:italic; color:#666; font-size:12px;">Se puede implementar por tag o material + tracking de seguimiento.</span>
                        </div>
                    </div>

                    <div class="contenedor_elemento">
                    	<div class="titulo_elemento">BANNER BRANDING</div>
                        <div class="contenedor_info_elemento">
                        	<div class="info_elemento"><b>Dimensi&oacute;n:</b> 300x560</div>
                            <div class="pipe_elemento">|</div>
                            <div class="info_elemento"><b>Formato:</b> HTML5 / JPG / GIF</div>
                            <div class="pipe_elemento">|</div>
                            <div class="info_elemento"><b>Peso m&aacute;ximo:</b> 45kb (JPG y GIF) / 100kb (HTML5)</div>
                            <div class="pipe_elemento">|</div>
                            <div class="ver_demo_elemento"><a href="demo.php?id=branding">VER DEMO</a></div>
                            <br /><span style="font-style:italic; color:#666; font-size:12px;">Requiere reserva.</span>
                            <br /><span style="font-style:italic; color:#666; font-size:12px;">Se puede implementar por tag o material + tracking de seguimiento.</span>
                        </div>
                    </div>

                    <div class="contenedor_elemento">
                    	<div class="titulo_elemento">BANNER CONTENT</div>
                        <div class="contenedor_info_elemento">
                        	<div class="info_elemento"><b>Dimensi&oacute;n:</b> 575x250</div>
                            <div class="pipe_elemento">|</div>
                            <div class="info_elemento"><b>Formato:</b> HTML5 / JPG / GIF</div>
                            <div class="pipe_elemento">|</div>
                            <div class="info_elemento"><b>Peso m&aacute;ximo:</b> 45kb (JPG y GIF) / 100kb (HTML5)</div>
                            <div class="pipe_elemento">|</div>
                            <div class="ver_demo_elemento"><a href="demo.php?id=content">VER DEMO</a></div>
                            <br /><span style="font-style:italic; color:#666; font-size:12px;">Requiere reserva.</span>
                            <br /><span style="font-style:italic; color:#666; font-size:12px;">Se puede implementar por tag o material + tracking de seguimiento.</span>
                        </div>
                    </div>

                    <div class="contenedor_elemento">
                    	<div class="titulo_elemento">ITT</div>
                        <div class="contenedor_info_elemento">
                        	<div class="info_elemento"><b>Dimensi&oacute;n:</b> 800x600</div>
                            <div class="pipe_elemento">|</div>
                            <div class="info_elemento"><b>Formato:</b> HTML5 / JPG / GIF</div>
                            <div class="pipe_elemento">|</div>
                            <div class="info_elemento"><b>Observaci&oacute;n:</b> Se integra autom&aacute;ticamente bot&oacute;n de cierre.</div>
                            <div class="pipe_elemento">|</div>
                            <div class="info_elemento"><b>Restricciones:</b> M&aacute;ximo 8 segundos de duraci&oacute;n.</div>
                            <div class="pipe_elemento">|</div>
                            <div class="ver_demo_elemento"><a href="demo.php?id=itt">VER DEMO</a></div>
                            <br /><span style="font-style:italic; color:#666; font-size:12px;">Requiere reserva.</span>
                            <br /><span style="font-style:italic; color:#666; font-size:12px;">Se debe enviar tracking de seguimiento.</span>
                        </div>
                    </div>

                    <div class="contenedor_elemento">
                    	<div class="titulo_elemento">CARRUSEL</div>
                        <div class="contenedor_info_elemento">
                        	
                            <div class="info_elemento">
                            	<b>Especificaciones:</b> Color de fondo que coincida con la gr&aacute;fica entregada.<br />
								2 gr&aacute;ficas, una para el Carrusel expandido y otra para el Carrusel cerrado.<br />
								Para los &iacute;tems del carrusel (mínimo 5) las medidas son 220x110px.<br />
								Las im&aacute;genes miniatura deben venir con descripci&oacute;n del producto inclu&iacute;da. 
                            </div><br />
                            <div class="info_elemento"><b>Peso m&aacute;ximo:</b> 45kb (JPG y GIF) / 100kb (HTML5)</div>
                            <div class="pipe_elemento">|</div>
                            <div class="ver_demo_elemento"><a href="demo.php?id=carrusel">VER DEMO</a></div>
                            <div class="pipe_elemento">|</div>
                            <div class="ver_demo_elemento"><a href="http://digital.mediosregionales.cl/carrusel/demo/index.php?dir=936" target="_blank">VER EJEMPLO</a></div>
                            <br /><span style="font-style:italic; color:#666; font-size:12px;">Requiere reserva.</span>
                            <br /><span style="font-style:italic; color:#666; font-size:12px;">Se debe enviar tracking de seguimiento.</span>
                        </div>
                    </div>

                    <div class="contenedor_elemento">
                    	<div class="titulo_elemento">BANNER CENTRAL</div>
                        <div class="contenedor_info_elemento">
                        	<div class="info_elemento"><b>Dimensi&oacute;n:</b> 650x125</div>
                            <div class="pipe_elemento">|</div>
                            <div class="info_elemento"><b>Formato:</b> HTML5 / JPG / GIF</div>
                            <div class="pipe_elemento">|</div>
                            <div class="info_elemento"><b>Peso m&aacute;ximo:</b> 45kb (JPG y GIF) / 100kb (HTML5)</div>
                            <div class="pipe_elemento">|</div>
                            <div class="ver_demo_elemento"><a href="demo.php?id=central">VER DEMO</a></div>
                            <br /><span style="font-style:italic; color:#666; font-size:12px;">Requiere reserva.</span>
                            <br /><span style="font-style:italic; color:#666; font-size:12px;">Se puede implementar por tag o material + tracking de seguimiento.</span>
                        </div>
                    </div>
                    
                    <div class="contenedor_elemento">
                    	<div class="titulo_elemento">BANNER BOX</div>
                        <div class="contenedor_info_elemento">
                        	<div class="info_elemento"><b>Dimensi&oacute;n:</b> 300x250</div>
                            <div class="pipe_elemento">|</div>
                            <div class="info_elemento"><b>Formato:</b> HTML5 / JPG / GIF</div>
                            <div class="pipe_elemento">|</div>
                            <div class="info_elemento"><b>Peso m&aacute;ximo:</b> 45kb (JPG y GIF) / 100kb (HTML5)</div>
                            <div class="pipe_elemento">|</div>
                            <div class="ver_demo_elemento"><a href="demo.php?id=box">VER DEMO</a></div>
                            <br /><span style="font-style:italic; color:#666; font-size:12px;">Requiere reserva.</span>
                            <br /><span style="font-style:italic; color:#666; font-size:12px;">Se puede implementar por tag o material + tracking de seguimiento.</span>
                        </div>
                    </div>

                    <div class="contenedor_elemento">
                    	<div class="titulo_elemento">BANNER DERECHA</div>
                        <div class="contenedor_info_elemento">
                        	<div class="info_elemento"><b>Dimensi&oacute;n:</b> 300x100</div>
                            <div class="pipe_elemento">|</div>
                            <div class="info_elemento"><b>Formato:</b> HTML5 / JPG / GIF</div>
                            <div class="pipe_elemento">|</div>
                            <div class="info_elemento"><b>Peso m&aacute;ximo:</b> 45kb (JPG y GIF) / 100kb (HTML5)</div>
                            <div class="pipe_elemento">|</div>
                            <div class="ver_demo_elemento"><a href="demo.php?id=derecha">VER DEMO</a></div>
                            <br /><span style="font-style:italic; color:#666; font-size:12px;">Requiere reserva.</span>
                            <br /><span style="font-style:italic; color:#666; font-size:12px;">Se puede implementar por tag o material + tracking de seguimiento.</span>
                        </div>
                    </div>

                    <div class="contenedor_elemento">
                    	<div class="titulo_elemento">PUBLIPOST</div>
                        <div class="contenedor_info_elemento">
                            <div class="info_elemento"><b>Formato:</b> Texto + 2 im&aacute;genes.</div>
                            <div class="pipe_elemento">|</div>
                            <div class="info_elemento"><b>Caracter&iacute;sticas:</b> 3 d&iacute;as destacado en el home (d&iacute;as a elecci&oacute;n)</div>
                            <div class="pipe_elemento">|</div>
                            <div class="ver_demo_elemento"><a href="demo.php?id=publipost">VER DEMO</a></div>
                            <div class="pipe_elemento">|</div>
                            <div class="ver_demo_elemento"><a href="http://www.soychile.cl/Santiago/Publirreportajes/2016/05/17/393736/ENAP-inicia-limpieza-y-anuncia-investigacion-interna-por-fuga-de-producto-en-Quintero.aspx" target="_blank">VER EJEMPLO</a></div>
                        	<br /><span style="font-style:italic; color:#666; font-size:12px;">Requiere reserva.</span>
                        </div>
                    </div>

                    <div class="contenedor_elemento">
                    	<div class="titulo_elemento">PUBLIPOST + BANNER</div>
                        <div class="contenedor_info_elemento">
                        	<div class="info_elemento"><b>Dimensi&oacute;n:</b> 728x125</div>
                            <div class="pipe_elemento">|</div>
                            <div class="info_elemento"><b>Formato:</b> Texto + 2 im&aacute;genes. Banner HTML5 / JPG / GIF</div>
                            <div class="pipe_elemento">|</div>
                            <div class="info_elemento"><b>Caracter&iacute;sticas:</b> 3 d&iacute;as destacado en el home (d&iacute;as a elecci&oacute;n)</div>
                            <div class="pipe_elemento">|</div>
                            <div class="ver_demo_elemento"><a href="demo.php?id=publipost_banner">VER DEMO</a></div>
                            <br /><span style="font-style:italic; color:#666; font-size:12px;">Requiere reserva.</span>
                        </div>
                    </div>

                    <div class="contenedor_elemento">
                    	<div class="titulo_elemento">BANNER BOX VIDEO</div>
                        <div class="contenedor_info_elemento">
                        	<div class="info_elemento"><b>Dimensi&oacute;n:</b> 300x250</div>
                            <div class="pipe_elemento">|</div>
                            <div class="info_elemento"><b>Formato:</b> Se debe enviar URL del video en Youtube.</div>
                            <div class="pipe_elemento">|</div>
                            <div class="ver_demo_elemento"><a href="demo.php?id=box_video">VER DEMO</a></div>
                            <br /><span style="font-style:italic; color:#666; font-size:12px;">Requiere reserva.</span>
                            <br /><span style="font-style:italic; color:#666; font-size:12px;">Se debe enviar tracking de seguimiento.</span>
                            <br /><span style="font-style:italic; color:#666; font-size:12px;">Tiene auto play sin sonido, el usuario lo puede activar (se replica el reproductor de Youtube).</span>
                        </div>
                    </div>

                    <div class="contenedor_elemento">
                    	<div class="titulo_elemento">PREROLL</div>
                        <div class="contenedor_info_elemento">
                            <div class="info_elemento"><b>Formato:</b> Se debe enviar URL del video en Youtube.</div>
                            <div class="pipe_elemento">|</div>
                            <div class="info_elemento"><b>Observaci&oacute;n:</b> Se puede saltar el anuncio a los 5 segundos.</div>
                            <div class="pipe_elemento">|</div>
                            <div class="ver_demo_elemento"><a href="demo.php?id=preroll">VER DEMO</a></div>
                            <br /><span style="font-style:italic; color:#666; font-size:12px;">Requiere reserva.</span>
                            <br /><span style="font-style:italic; color:#666; font-size:12px;">Se debe enviar tracking de seguimiento.</span>
                        </div>
                    </div>
                </div>
            	<div id="tab-3">
            		<div class="contenedor_elemento">
                    	<div class="titulo_elemento">ITT</div>
                        <div class="contenedor_info_elemento">
                        	<div class="info_elemento"><b>Dimensi&oacute;nes:</b> Horizontal 480x220 | Vertical 320x370</div>
                            <div class="pipe_elemento">|</div>
                            <div class="info_elemento"><b>Formato:</b> HTML5 / JPG / GIF</div>
                            <div class="pipe_elemento">|</div>
                            <div class="info_elemento"><b>Peso m&aacute;ximo:</b> 45kb (JPG y GIF) / 100kb (HTML5)</div>
                            <div class="pipe_elemento">|</div>
                            <div class="info_elemento"><b>Observaci&oacute;n:</b> Se integra autom&aacute;ticamente bot&oacute;n de cierre.</div>
                            <div class="pipe_elemento">|</div>
                            <div class="ver_demo_elemento"><a href="demo_mobile.php?id=itt">VER DEMO</a></div>
                            <br /><span style="font-style:italic; color:#666; font-size:12px;">Requiere reserva.</span>
                            <br /><span style="font-style:italic; color:#666; font-size:12px;">Se debe enviar tracking de seguimiento.</span>
                        </div>
                    </div>
            		<div class="contenedor_elemento">
                    	<div class="titulo_elemento">BANNER HEAD / INTERNOTICIA</div>
                        <div class="contenedor_info_elemento">
                        	<div class="info_elemento"><b>Dimensi&oacute;n:</b> 320x100</div>
                            <div class="pipe_elemento">|</div>
                            <div class="info_elemento"><b>Formato:</b> HTML5 / JPG / GIF</div>
                            <div class="pipe_elemento">|</div>
                            <div class="info_elemento"><b>Peso m&aacute;ximo:</b> 45kb (JPG y GIF) / 100kb (HTML5)</div>
                            <div class="pipe_elemento">|</div>
                            <div class="ver_demo_elemento"><a href="demo_mobile.php?id=head">VER DEMO</a></div>
                            <br /><span style="font-style:italic; color:#666; font-size:12px;">Requiere reserva.</span>
                            <br /><span style="font-style:italic; color:#666; font-size:12px;">Se debe enviar tracking de seguimiento.</span>
                        </div>
                    </div>

                    <div class="contenedor_elemento">
                    	<div class="titulo_elemento">BANNER CONTENT</div>
                        <div class="contenedor_info_elemento">
                        	<div class="info_elemento"><b>Dimensi&oacute;n:</b> 300x250</div>
                            <div class="pipe_elemento">|</div>
                            <div class="info_elemento"><b>Formato:</b> HTML5 / JPG / GIF</div>
                            <div class="pipe_elemento">|</div>
                            <div class="info_elemento"><b>Peso m&aacute;ximo:</b> 45kb (JPG y GIF) / 100kb (HTML5)</div>
                            <div class="pipe_elemento">|</div>
                            <div class="ver_demo_elemento"><a href="demo_mobile.php?id=content">VER DEMO</a></div>
                            <br /><span style="font-style:italic; color:#666; font-size:12px;">Requiere reserva.</span>
                            <br /><span style="font-style:italic; color:#666; font-size:12px;">Se debe enviar tracking de seguimiento.</span>
                        </div>
                    </div>
            	</div>

            	<div id="tab-4">
            		<div class="contenedor_elemento">
                    	<div class="titulo_elemento">BIG BANNER IMAGEN + VIDEO</div>
                        <div class="contenedor_info_elemento">
                        	<div class="info_elemento"><b>Dimensi&oacute;nes:</b> 2 Im&aacute;genes de 600x300 + Video (MP4)</div>
                            <div class="pipe_elemento">|</div>
                            <div class="info_elemento"><b>Formato:</b> GIF Animado / JPG / MP4</div>
                            <div class="pipe_elemento">|</div>
                            <div class="info_elemento"><b>Observaci&oacute;n:</b> Se integra autom&aacute;ticamente bot&oacute;n de cierre.</div>
                            <div class="pipe_elemento">|</div>
                            <div class="info_elemento"><b>Peso m&aacute;ximo:</b> 45kb (JPG y GIF)</div>
                            <div class="pipe_elemento">|</div>
                            <div class="ver_demo_elemento"><a href="demo.php?id=bigbanner_video">VER DEMO</a></div>
                            <br /><span style="font-style:italic; color:#666; font-size:12px;">Requiere reserva.</span>
                            <br /><span style="font-style:italic; color:#666; font-size:12px;">Se debe enviar tracking de seguimiento.</span>
                        </div>
                    </div>
                    <div class="contenedor_elemento">
                    	<div class="titulo_elemento">BIG BANNER IMAGEN</div>
                        <div class="contenedor_info_elemento">
                        	<div class="info_elemento"><b>Dimensi&oacute;nes:</b> 600x900</div>
                            <div class="pipe_elemento">|</div>
                            <div class="info_elemento"><b>Formato:</b> JPG / GIF</div>
                            <div class="pipe_elemento">|</div>
                            <div class="info_elemento"><b>Observaci&oacute;n:</b> Se integra autom&aacute;ticamente bot&oacute;n de cierre.</div>
                            <div class="pipe_elemento">|</div>
                            <div class="info_elemento"><b>Peso m&aacute;ximo:</b> 45kb</div>
                            <div class="pipe_elemento">|</div>
                            <div class="ver_demo_elemento"><a href="demo.php?id=bigbanner">VER DEMO</a></div>
                            <br /><span style="font-style:italic; color:#666; font-size:12px;">Requiere reserva.</span>
                            <br /><span style="font-style:italic; color:#666; font-size:12px;">Se debe enviar tracking de seguimiento.</span>
                        </div>
                    </div>
            	</div>
        
            </div>
    
		</div><!--cierra contenido-->
    </div><!--cierra fila contenido-->

    <!-- Responsive Tabs JS -->
    <script src="js/jquery.responsiveTabs.js" type="text/javascript"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            var $tabs = $('#horizontalTab');
            $tabs.responsiveTabs({
                rotate: false,
                startCollapsed: 'accordion',
                collapsible: 'accordion',
                setHash: true,
                //disabled: [3,4],
                activate: function(e, tab) {
                    $('.info').html('Tab <strong>' + tab.id + '</strong> activated!');
                },
                activateState: function(e, state) {
                    //console.log(state);
                    $('.info').html('Switched from <strong>' + state.oldState + '</strong> state to <strong>' + state.newState + '</strong> state!');
                }
            });

            

        });
    </script>

<?php include('footer.php');?>	