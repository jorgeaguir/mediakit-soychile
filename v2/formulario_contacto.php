<div class="caja_alertas">
	<div class="btn_cerrar_form" onClick="cerrar()" style="cursor:pointer">X</div>
	<div class="contenedor_titulo_form">Completa los datos del formulario y te contactaremos.</div>
	<div class="texto_caja_alerta">
		<form action="<?php echo $_SERVER['REQUEST_URI']?>" method="post" enctype="multipart/form-data">
			<div class="contenedor_item_form">
				<div class="texto_item_form">Nombre</div>
				<div class="contenedor_campo_item_form"><input name="nombre" type="text" class="campo_item_form"/></div>
			</div>
			<div class="contenedor_item_form">
				<div class="texto_item_form">Correo</div>
				<div class="contenedor_campo_item_form"><input name="mail" type="text" class="campo_item_form"/></div>
			</div>
			<div class="contenedor_item_form">
				<div class="texto_item_form">Tel&eacute;fono</div>
				<div class="contenedor_campo_item_form"><input name="telefono" type="text" class="campo_item_form"/></div>
			</div>
			<!--<div class="contenedor_item_form">
				<div class="texto_item_form">&Aacute;rea de contacto</div>
				<div class="contenedor_campo_item_form">
					<select class="campo_item_form">
						<option>Selecciona un &aacute;rea de contacto</option>
						<option>Soychile</option>
						<option>Autolocal</option>
					</select>
				</div>
			</div>-->
			<div class="contenedor_item_form">
				<div class="texto_item_form">Mensaje</div>
				<div class="contenedor_campo_item_form"><textarea name="comentarios" class="area_item_form"></textarea></div>
			</div>
			<div class="contenedor_item_form">
				<div class="texto_item_form"></div>
				<div class="contenedor_campo_item_form"><input type="submit" value="Enviar" name="boton_enviar" class="boton_alerta"/></div>
			</div>
		</form>
	</div>
	
</div>