<?php 
$url_volver='index.php';
include('header.php'); 
?>	

	<div class="fila_contenido">
    	<div class="contenido">

	    	<div class="container_scroll">
	        	<div class="columna_scroll">
	        		<div class="sticky">
		        		<div id="menu">
		            
			            	<div class="encabezado_menu_layout">IR A LA MEDIDA</div>
			            	<ul class="lista_menu_layout">
			            		<?php if(isset($_GET['id']) && $_GET['id'] == 'desktop'){?>
			            		<li class="item_lista_menu_layout"><a href="#head">Head</a></li>
			            		<li class="item_lista_menu_layout"><a href="#branding1">Branding superior</a></li>
			            		<li class="item_lista_menu_layout"><a href="#content1">Content superior</a></li>
			            		<li class="item_lista_menu_layout"><a href="#box1">Box superior</a></li>
			            		<li class="item_lista_menu_layout"><a href="#derecha1">Derecha superior</a></li>
			            		<li class="item_lista_menu_layout"><a href="#central1">Central superior</a></li>
			            		<li class="item_lista_menu_layout"><a href="#box2">Box medio</a></li>
			            		<li class="item_lista_menu_layout"><a href="#derecha2">Derecha medio</a></li>
			            		<li class="item_lista_menu_layout"><a href="#content2">Content medio 1</a></li>
			            		<li class="item_lista_menu_layout"><a href="#branding2">Branding inferior</a></li>
			            		<li class="item_lista_menu_layout"><a href="#central2">Central medio 1</a></li>
			            		<li class="item_lista_menu_layout"><a href="#content3">Content medio 2</a></li>
			            		<li class="item_lista_menu_layout"><a href="#central3">Central medio 2</a></li>
			            		<li class="item_lista_menu_layout"><a href="#content4">Content inferior</a></li>
			            		<li class="item_lista_menu_layout"><a href="#central4">Central inferior</a></li>
			            		<?php }elseif(isset($_GET['id']) && $_GET['id'] == 'mobile'){?>
			            		<li class="item_lista_menu_layout"><a href="#head_movil">Head</a></li>
			            		<li class="item_lista_menu_layout"><a href="#content_movil">Content</a></li>
			            		<?php }?>
			            	</ul>

		            	</div>
	            	</div>
	            </div>
	        </div>

	        <?php if(isset($_GET['id']) && $_GET['id'] == 'desktop'){?>
            <div class="img_layout">
            	<div class="punto_layout" id="head"></div>
            	<div class="punto_layout" id="branding1"></div>
            	<div class="punto_layout" id="content1"></div>
            	<div class="punto_layout" id="box1"></div>
            	<div class="punto_layout" id="derecha1"></div>
            	<div class="punto_layout" id="central1"></div>
            	<div class="punto_layout" id="box2"></div>
            	<div class="punto_layout" id="derecha2"></div>
            	<div class="punto_layout" id="content2"></div>
            	<div class="punto_layout" id="branding2"></div>
            	<div class="punto_layout" id="central2"></div>
            	<div class="punto_layout" id="content3"></div>
            	<div class="punto_layout" id="central3"></div>
            	<div class="punto_layout" id="content4"></div>
            	<div class="punto_layout" id="central4"></div>
            </div>
            <?php }elseif(isset($_GET['id']) && $_GET['id'] == 'mobile'){?>
	        <div class="contenedor_img_layout_movil">
	            <div class="img_layout_movil">
	            	<div class="punto_layout" id="head_movil"></div>
	            	<div class="punto_layout" id="content_movil"></div>
	            </div>
	        </div>
            <?php }?>


		</div><!--cierra contenido-->
    </div><!--cierra fila contenido-->

    <div id="fin" style="width:100%; height:10px; float:left;"></div>
    


<?php include('footer.php');?>	