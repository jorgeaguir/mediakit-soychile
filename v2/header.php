<?php 
if(isset($_POST['boton_enviar'])){
		
	$nombre = $_POST['nombre'];
	$telefono = $_POST['telefono'];
	$mail = $_POST['mail'];
	$comentarios = $_POST['comentarios'];
	
	
	$header = 'From: Soychile <newsletter@autolocal.com>' . " \r\n";
	$header .= "X-Mailer: PHP/" . phpversion() . " \r\n";
	$header .= "Mime-Version: 1.0 \r\n";
	$header .= "Content-Type: text/html";
	
	
	
	$mensaje = '<table width="500" border="0" cellspacing="0" cellpadding="0" style="border:1px solid #333; background-color:#fff;" align="center">
  <tr>
    <td colspan="3" style="background-color:#f00; border-bottom:1px solid #333; text-align:center; padding:5px 0 5px 0;"><img src="http://mediakit.mediosregionales.cl/images/logo_soychile.png" alt="soychile" style="font-family:Arial, Helvetica, sans-serif; color:#fff; font-weight:bold; font-size:16px;" /></td>
  </tr>
  <tr>
    <td width="15" style="padding:20px 0 10px 0;">&nbsp;</td>
    <td width="470" style="padding:20px 0 10px 0;">
    	<span style="font-family:Arial, Helvetica, sans-serif; color:#999; font-weight:bold; font-size:18px;">Contacto desde Mediakit</span>
    </td>
    <td width="15" style="padding:20px 0 10px 0;">&nbsp;</td>
  </tr>
  
  <tr>
    <td width="15" style="padding:10px 0 10px 0;">&nbsp;</td>
    <td width="470" style="padding:10px 0 10px 0;">
    	<table width="470" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="100" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#000; font-weight:bold; padding:5px 0 5px 0;">Nombre:</td>
            <td width="370" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#000; padding:5px 0 5px 0;">'.$nombre.'</td>
          </tr>
          <tr>
            <td width="100" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#000; font-weight:bold; padding:5px 0 5px 0;">E-mail:</td>
            <td width="370" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#000; padding:5px 0 5px 0;">'.$mail.'</td>
          </tr>
          <tr>
            <td width="100" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#000; font-weight:bold; padding:5px 0 5px 0;">Tel&eacute;fono:</td>
            <td width="370" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#000; padding:5px 0 5px 0;">'.$telefono.'</td>
          </tr>
          <tr>
            <td width="100" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#000; font-weight:bold; padding:5px 0 5px 0;">Mensaje:</td>
            <td width="370" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#000; padding:5px 0 5px 0;">'.$comentarios.'</td>
          </tr>
        </table>	
    </td>
    <td width="15" style="padding:10px 0 10px 0;">&nbsp;</td>
  </tr>
  <tr>
    <td width="15">&nbsp;</td>
    <td width="470">&nbsp;</td>
    <td width="15">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" style="background-color:#000; border-bottom:1px solid #333; text-align:center; padding:5px 0 5px 0; color:#fff;">
    	<span style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#fff;">2016 | Soychile</span>
    </td>
  </tr>
</table>';
	
	
	
	$para = 'natalia.navarro@mediosregionales.cl'; 
	$para2 = 'francisca.duran@mediosregionales.cl'; 
	$para3 = 'juan.arteaga@mediosregionales.cl'; 
	$asunto = 'Contacto desde Mediakit';
	
	mail($para, $asunto, utf8_decode($mensaje), $header);
	mail($para2, $asunto, utf8_decode($mensaje), $header);
	mail($para3, $asunto, utf8_decode($mensaje), $header);

	$mensaje_ok='Muchas gracias por escribirnos,<br />tu mensaje se ha enviado correctamente.';

}
?>


<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Soychile | Mediakit</title>

<link rel="icon" type="image/png" href="favicon.png" />

<script src="js/jquery-2.1.0.min.js"></script>
<script type="text/javascript" src="js/transicion.js"></script>
<script src="js/scroll.js" type="text/javascript"></script>

<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,700,700italic' rel='stylesheet' type='text/css'>


<link type="text/css" rel="stylesheet" href="css/responsive-tabs.css" />
<link type="text/css" rel="stylesheet" href="css/style.css" />


</head>
<body>

<script type="text/javascript">
	function mostrar(){
		document.getElementById('caja_alerta').style.display='table';
		document.getElementById('fondo_alerta').style.display='table';
	}
	function cerrar(){
		document.getElementById('caja_alerta').style.display='none';
		document.getElementById('fondo_alerta').style.display='none';
	}	
</script>

<div id="fondo_alerta" class="fondo_alertas" style="display:none;">
</div>

<table width="100%" id="caja_alerta" cellspacing="0" cellpadding="0" border="0" class="tabla_alertas" style="display:none;">
	<tr>
		<td>
			<?php include('formulario_contacto.php');?>
		</td>
	</tr>
</table>

<?php 
if(isset ($mensaje_ok)){?>
<script type="text/javascript">
$(document).ready(function() {
    setTimeout(function() {
        $(".alerta_total").fadeOut(500);
    },2000);
});
</script>
<div class="alerta_total">
<div id="fondo_alerta" class="fondo_alertas">
</div>

<table width="100%" id="caja_alerta" cellspacing="0" cellpadding="0" border="0" class="tabla_alertas">
	<tr>
		<td>
			<div class="caja_alertas">
				
				<div class="img_gracias_form"><img src="images/check_alerta.png" /></div>
				<div class="gracias_form"><?php echo $mensaje_ok?></div>
				
			</div>
		</td>
	</tr>
</table>
</div>
<?php }?>

<div class="contenedor_total">
    
	<div class="contenedor_header">
    	<div class="contenido_header">
        	<div style="width:113px; height:auto; float:left; padding-bottom:5px;">
        		<a href="index.php"><img src="images/logo_soychile.png" border="0" /></a>
        	</div>
        	<div class="boton_tarifario">
        		<div class="icono_tarifario"><a href="descargar_archivo.php?id=tarifario"><img src="images/descargar.png" /></a></div>
        		<div class="link_tarifario"><a href="descargar_archivo.php?id=tarifario">DESCARGAR TARIFARIO</a></div>
        	</div>
        </div>
    </div>

    <div class="fila_contenido">
    	<div class="contenido">
			<div class="contenedor_contacto" onClick="mostrar()" style="cursor:pointer"><img src="images/contacto.png" /> CONTACTO</div>
			<?php if(strpos($_SERVER['REQUEST_URI'],"layout") !== false || strpos($_SERVER['REQUEST_URI'],"demo") !== false){?>
				<div class="contenedor_volver"><a href="<?php echo $url_volver?>"><img src="images/volver.png" /> VOLVER</a></div>
			<?php }?>
		</div>
	</div>