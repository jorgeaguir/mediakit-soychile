<?php 
if(isset($_GET['id']) && $_GET['id'] != ''){
	if($_GET['id']=='head'){
		$imagen='head_728x125.jpg';
		$titulo='BANNER HEAD 728x125';
		$url_volver='index.php#tab-2';
		$logos=true;
	}elseif($_GET['id']=='head_expandible'){
		$imagen='head_expandible_728x300.jpg';
		$titulo='BANNER HEAD EXPANDIBLE 728x125 &rarr; 728x300';
		$url_volver='index.php#tab-2';
		$logos=true;
	}elseif($_GET['id']=='branding'){
		$imagen='branding_300x560.jpg';
		$titulo='BANNER BRANDING 300x560';
		$url_volver='index.php#tab-2';
		$logos=true;
	}elseif($_GET['id']=='content'){
		$imagen='content_575x250.jpg';
		$titulo='BANNER CONTENT 575x250';
		$url_volver='index.php#tab-2';
		$logos=true;
	}elseif($_GET['id']=='box'){
		$imagen='box_300x250.jpg';
		$titulo='BANNER BOX 300x250';
		$url_volver='index.php#tab-2';
		$logos=true;
	}elseif($_GET['id']=='itt'){
		$imagen='itt_800x600_barra.png';
		$titulo='BANNER ITT 800x600 + BARRA 650x125';
		$url_volver='index.php#tab-2';
		$logos=true;
	}elseif($_GET['id']=='derecha'){
		$imagen='derecha_300x100.jpg';
		$titulo='BANNER DERECHA 300x100';
		$url_volver='index.php#tab-2';
		$logos=true;
	}elseif($_GET['id']=='central'){
		$imagen='central_650x125.jpg';
		$titulo='BANNER CENTRAL 650x125';
		$url_volver='index.php#tab-2';
		$logos=true;
	}elseif($_GET['id']=='carrusel'){
		$imagen='carrusel.jpg';
		$titulo='CARRUSEL';
		$url_volver='index.php#tab-2';
		$logos=true;
	}elseif($_GET['id']=='publipost'){
		$imagen='publipost.jpg';
		$titulo='PUBLIPOST (Vista en Home, m&aacute;s abajo encontrar&aacute;s la vista interior)';
		$imagen_2='publipost_interior.jpg';
		$titulo_2='PUBLIPOST (Vista interior)';
		$url_volver='index.php#tab-2';
		$logos=true;
	}elseif($_GET['id']=='publipost_banner'){
		$imagen='publipost.jpg';
		$titulo='PUBLIPOST + BANNER (Vista en Home, m&aacute;s abajo encontrar&aacute;s la vista interior)';
		$imagen_2='publipost_interior_banner.jpg';
		$titulo_2='PUBLIPOST + BANNER (Vista interior)';
		$url_volver='index.php#tab-2';
		$logos=true;
	}elseif($_GET['id']=='box_video'){
		$imagen='300x250_video.gif';
		$titulo='BOX VIDEO 300x250';
		$url_volver='index.php#tab-2';
		$logos=true;
	}elseif($_GET['id']=='preroll'){
		$imagen='preroll.gif';
		$titulo='PREROLL';
		$url_volver='index.php#tab-2';
		$logos=true;
	}elseif($_GET['id']=='bigbanner_video'){
		$imagen='bigbanner_video.gif';
		$titulo='BIG BANNER IMAGEN + VIDEO';
		$url_volver='index.php#tab-4';
		$logos=false;
	}elseif($_GET['id']=='bigbanner'){
		$imagen='bigbanner_600x900.jpg';
		$titulo='BIG BANNER IMAGEN';
		$url_volver='index.php#tab-4';
		$logos=false;
	}
}else{
	header('Location:index.php');
}


include('header.php');
?>	

    <div class="fila_contenido">
    	<div class="contenido" style="font-weight:bold; text-align:center; font-size:21px;">
    		<?php echo $titulo?>
    	</div>
    </div>

	<div class="fila_contenido">
    	<div class="contenido">

            <div class="contenedor_img_demo"><img src="images/<?php echo $imagen;?>" width="100%"/></div>
    
		</div><!--cierra contenido-->
    </div><!--cierra fila contenido-->


    <?php if(isset($_GET['id']) && $_GET['id'] == 'publipost' || $_GET['id'] == 'publipost_banner'){?>
    <div class="fila_contenido">
    	<div class="contenido" style="font-weight:bold; text-align:center; font-size:21px;">
    		<?php echo $titulo_2?>
    	</div>
    </div>

	<div class="fila_contenido">
    	<div class="contenido">

            <div class="contenedor_img_demo"><img src="images/<?php echo $imagen_2;?>" width="100%"/></div>
    
		</div><!--cierra contenido-->
    </div><!--cierra fila contenido-->
    <?php }?>

    <?php if($logos == true){?>
    <div class="fila_contenido">
    	<div class="contenido">
    		<ul style="width:100%; height:auto; float:left; padding:0; text-align:center;">
    			<li class="item_lista_logos"><img src="images/logos/soychile.gif" width="100%" /></li>
    			<li class="item_lista_logos"><img src="images/logos/soyarica.gif" width="100%" /></li>
    			<li class="item_lista_logos"><img src="images/logos/soyiquique.gif" width="100%" /></li>
    			<li class="item_lista_logos"><img src="images/logos/soycalama.gif" width="100%" /></li>
    			<li class="item_lista_logos"><img src="images/logos/soyantofagasta.gif" width="100%" /></li>
    			<li class="item_lista_logos"><img src="images/logos/soycopiapo.gif" width="100%" /></li>
    			<li class="item_lista_logos"><img src="images/logos/soyvalparaiso.gif" width="100%" /></li>
    			<li class="item_lista_logos"><img src="images/logos/soyquillota.gif" width="100%" /></li>
    			<li class="item_lista_logos"><img src="images/logos/soysanantonio.gif" width="100%" /></li>
    			<li class="item_lista_logos"><img src="images/logos/soychillan.gif" width="100%" /></li>
    			<li class="item_lista_logos"><img src="images/logos/soysancarlos.gif" width="100%" /></li>
    			<li class="item_lista_logos"><img src="images/logos/soytome.gif" width="100%" /></li>
    			<li class="item_lista_logos"><img src="images/logos/soytalcahuano.gif" width="100%" /></li>
    			<li class="item_lista_logos"><img src="images/logos/soyconcepcion.gif" width="100%" /></li>
    			<li class="item_lista_logos"><img src="images/logos/soycoronel.gif" width="100%" /></li>
    			<li class="item_lista_logos"><img src="images/logos/soyarauco.gif" width="100%" /></li>
    			<li class="item_lista_logos"><img src="images/logos/soytemuco.gif" width="100%" /></li>
    			<li class="item_lista_logos"><img src="images/logos/soyvaldivia.gif" width="100%" /></li>
    			<li class="item_lista_logos"><img src="images/logos/soyosorno.gif" width="100%" /></li>
    			<li class="item_lista_logos"><img src="images/logos/soypuertomontt.gif" width="100%" /></li>
    			<li class="item_lista_logos"><img src="images/logos/soychiloe.gif" width="100%" /></li>
    		</ul>
    	</div>
    </div>
    <?php }?>

<?php include('footer.php');?>	