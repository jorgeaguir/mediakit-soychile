<?php 
    $notaDestacada = "<div class=\"nota-destacada\"><div class=\"media\"></div><div class=\"text text-1\"></div><div class=\"text text-2\"></div></div>";
    $notaNormal =  "<div class=\"nota-normal\"><div class=\"text\"> <div class=\"text-1\"></div> <div class=\"text-2\"></div></div><div class=\"media\"></div></div>"
?>
<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Soychile | Mediakit</title>

<link rel="icon" type="image/png" href="favicon.png" />

<script src="js/jquery-2.1.0.min.js"></script>
<script type="text/javascript" src="js/transicion.js"></script>
<script src="js/scroll.js" type="text/javascript"></script>

<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,700,700italic' rel='stylesheet' type='text/css'>


<link type="text/css" rel="stylesheet" href="css/estilos_02.css" />
<link type="text/css" rel="stylesheet" href="css/demo2.css" />
<script>     
    var OA_zones = {'banner_branding_superior': '61'};
</script>
<script src='http://pa1.adxion.com/www/delivery/spcjs.php'></script>

</head>
<body>
<header>
    <div class="diario-head">
        
    </div>
    <div class="content-head">
        <img src="http://www.soychile.cl/assets/escritorio/img/ciudades/10.png" alt="logo soychile">
    </div>
</header>
<section>
    <div class="diario-body">
        <?php  for($i = 0; $i <= 5; $i++) { ?>
            <div class="diario-page"></div>
        <?php } ?>
    </div>
    <div class="content-body">
        <div class="flujo">
            <?php  for($i = 0; $i < 3; $i++) { echo $notaDestacada; }?>
            
            <?php  for($i = 0; $i <= 7; $i++) { 
                $x = rand(1,4);
                if ($x < 3 ) {
                    echo $notaDestacada;
                } else {
                    echo $notaNormal;
                }
             } ?>
      
            
     
        </div>
        <div class="sidebar">


            <div class="banner-branding">
             <script>
                OA_show('branding');
            </script>
            </div>

            <div class="block"></div>
            <?php  for($i = 0; $i < 3; $i++) { ?>
            <div class="nota-sidebar">
                <div class="media">
                    <div class="text text-1"></div>
                    <div class="text text-2"></div>
                </div>
            </div>
            <?php } ?>
        </div>
    </div>
</section>
</body>
</html>