<?php include('header.php');?>	
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.7";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>


	<div id="portada" style="width:100%; min-height:100%; float:left; background-color:#fff; padding-bottom:10px;">
		<div class="contenido_portada">
			<div class="izq_portada"><img src="images/formatos.jpg" width="100%" /></div>
			<div class="der_portada">
				<a href="http://www.soychile.cl" target="_blank"><img src="images/soychile.png" class="logo-main" /></a><br /><br />
				<b>LA RED DE SITIOS REGIONALES QUE INFORMA Y ACERCA A LOS CHILENOS</b><br />
				Todo el acontecer del pa&iacute;s minuto a minuto, unificado en una red de 21 sitios con informaci&oacute;n local y cercana para cada regi&oacute;n.<br /><br />

				Generamos alcance nacional y nuestras audiencias est&aacute;n segmentadas org&aacute;nicamente gracias a su contenido hiperlocal.<br /><br />

				<ul style="width:100%; height:auto;text-align:center;">
					<li class="item_lista_logos_home"><a href="http://www.soychile.cl" target="_blank"><img src="images/logos/soychile.gif" width="100%" /></a></li>
	    			<li class="item_lista_logos_home"><a href="http://www.soychile.cl/arica/" target="_blank"><img src="images/logos/soyarica.gif" width="100%" /></a></li>
	    			<li class="item_lista_logos_home"><a href="http://www.soychile.cl/iquique/" target="_blank"><img src="images/logos/soyiquique.gif" width="100%" /></a></li>
	    			<li class="item_lista_logos_home"><a href="http://www.soychile.cl/calama/" target="_blank"><img src="images/logos/soycalama.gif" width="100%" /></a></li>
	    			<li class="item_lista_logos_home"><a href="http://www.soychile.cl/antofagasta/" target="_blank"><img src="images/logos/soyantofagasta.gif" width="100%" /></a></li>
	    			<li class="item_lista_logos_home"><a href="http://www.soychile.cl/copiapo/" target="_blank"><img src="images/logos/soycopiapo.gif" width="100%" /></a></li>
	    			<li class="item_lista_logos_home"><a href="http://www.soychile.cl/valparaiso/" target="_blank"><img src="images/logos/soyvalparaiso.gif" width="100%" /></a></li>
	    			<li class="item_lista_logos_home"><a href="http://www.soychile.cl/quillota/" target="_blank"><img src="images/logos/soyquillota.gif" width="100%" /></a></li>
	    			<li class="item_lista_logos_home"><a href="http://www.soychile.cl/san-antonio/" target="_blank"><img src="images/logos/soysanantonio.gif" width="100%" /></a></li>
	    			<li class="item_lista_logos_home"><a href="http://www.soychile.cl/chillan/" target="_blank"><img src="images/logos/soychillan.gif" width="100%" /></a></li>
	    			<li class="item_lista_logos_home"><a href="http://www.soychile.cl/san-carlos/" target="_blank"><img src="images/logos/soysancarlos.gif" width="100%" /></a></li>
	    			<li class="item_lista_logos_home"><a href="http://www.soychile.cl/tome/" target="_blank"><img src="images/logos/soytome.gif" width="100%" /></a></li>
	    			<li class="item_lista_logos_home"><a href="http://www.soychile.cl/talcahuano/" target="_blank"><img src="images/logos/soytalcahuano.gif" width="100%" /></a></li>
	    			<li class="item_lista_logos_home"><a href="http://www.soychile.cl/concepcion/" target="_blank"><img src="images/logos/soyconcepcion.gif" width="100%" /></a></li>
	    			<li class="item_lista_logos_home"><a href="http://www.soychile.cl/coronel/" target="_blank"><img src="images/logos/soycoronel.gif" width="100%" /></a></li>
	    			<li class="item_lista_logos_home"><a href="http://www.soychile.cl/arauco/" target="_blank"><img src="images/logos/soyarauco.gif" width="100%" /></a></li>
	    			<li class="item_lista_logos_home"><a href="http://www.soychile.cl/temuco/" target="_blank"><img src="images/logos/soytemuco.gif" width="100%" /></a></li>
	    			<li class="item_lista_logos_home"><a href="http://www.soychile.cl/valdivia/" target="_blank"><img src="images/logos/soyvaldivia.gif" width="100%" /></a></li>
	    			<li class="item_lista_logos_home"><a href="http://www.soychile.cl/osorno/" target="_blank"><img src="images/logos/soyosorno.gif" width="100%" /></a></li>
	    			<li class="item_lista_logos_home"><a href="http://www.soychile.cl/puerto-montt/" target="_blank"><img src="images/logos/soypuertomontt.gif" width="100%" /></a></li>
	    			<li class="item_lista_logos_home"><a href="http://www.soychile.cl/chiloe/" target="_blank"><img src="images/logos/soychiloe.gif" width="100%" /></a></li>
				</ul>

			</div>

			<div style="width:100%; height:auto; float:left; font-size:22px;text-align:center;">
				<p style="width:100%; font-weight:bold;">¿CU&Aacute;L ES TU OBJETIVO? TENEMOS UN FORMATO PARA CADA UNO DE ELLOS</p>
				<ul style="width:100%; height:auto; margin-top:10px; text-align:center; list-style:none;">
					<li style="width:140px; height:140px; display:inline-block; margin:10px;">
						<div style="width:140px; height:100px;"><img src="images/icono_branding.jpg" /></div>
						<div style="width:140px; height:40px; font-size:16px;">Branding</div>
					</li>
					<li style="width:140px; height:140px; display:inline-block; margin:10px;">
						<div style="width:140px; height:100px;"><img src="images/icono_performance.jpg" /></div>
						<div style="width:140px; height:40px; font-size:16px;">Performance</div>
					</li>
					<li style="width:140px; height:140px; display:inline-block; margin:10px;">
						<div style="width:140px; height:100px;"><img src="images/icono_video.jpg" /></div>
						<div style="width:140px; height:40px; font-size:16px;">Video</div>
					</li>
					<li style="width:140px; height:140px; display:inline-block; margin:10px;">
						<div style="width:140px; height:100px;"><img src="images/icono_contenido.jpg" /></div>
						<div style="width:140px; height:40px; font-size:16px;">Contenido</div>
					</li>
				</ul>
			</div>

			<div style="width:100%; height:auto; float:left; font-size:18px; margin-top:5px; text-align:center;">
				<ul style="width:100%; height:auto; text-align:center; list-style:none; vertical-align:middle;">
					<li style="height:auto; display:inline-block; margin:10px 20px 10px 20px;">
						<div style="width:30px; height:30px; float:left;"><a href="http://www.facebook.com/soychilecl" target="_blank" style="color:#000; text-decoration:none;"><img src="images/facebook.jpg" /></a></div>
						<div style="height:auto; float:left; font-size:16px; padding-top:4px; margin-left:10px; text-align:left;">
                           <div class="fb-like" data-href="https://www.facebook.com/soychilecl" data-layout="button" data-action="like" data-size="small" data-show-faces="false" data-share="false"></div>
                        </div>
					</li>

					<li style="height:auto; display:inline-block; margin:10px 20px 10px 20px;">
						<div style="width:30px; height:30px; float:left;"><a href="http://www.twitter.com/soychilecl" target="_blank" style="color:#000; text-decoration:none;"><img src="images/twitter.jpg" /></a></div>
						<div style=" height:auto; float:left; font-size:16px; padding-top:4px; margin-left:10px; text-align:left;">
                            <a href="https://twitter.com/soychilecl" class="twitter-follow-button" data-show-count="false" data-lang="es">Seguir a @soychilecl</a> <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
                        </div>
					</li>

					<!-- <li style="width:270px; height:50px; display:inline-block; margin:10px 20px 10px 20px;">
						<div style="width:140px; height:50px; float:left;"><a href="http://www.soychile.cl" target="_blank" style="color:#000; text-decoration:none;"><img src="images/link_soychile.jpg" /></a></div>
						<div style="width:120px; height:auto; float:left; font-size:16px; padding-top:12px; margin-left:10px; text-align:left;"><a href="http://www.soychile.cl" target="_blank" style="color:#000; text-decoration:none;">soychile.cl</a></div>
					</li> -->
				</ul>
			</div>

		</div>
	</div>

    

	<div id="formatos" class="fila_contenido margen_formatos" style="min-height:100%;">
    	<div class="contenido">

            <!--Horizontal Tab-->
            <div id="horizontalTab">
                <ul>
                    <li><a href="#tab-1">Layout</a></li>
                    <li><a href="#tab-2">Formatos Desktop</a></li>
                    <li><a href="#tab-3">Formatos Mobile</a></li>
                    <li><a href="#tab-4">Papel Digital</a></li>
                </ul>
        
                <div id="tab-1">
                    <ul style="width:100%; height:auto; padding:0; margin:0; text-align:center;">
                    	<li style="width:180px; height:auto; list-style:none; display:inline-block; border:1px solid #666; margin:20px;">
                    		<div style="width:180px; height:180px;"><a href="layout.php?id=desktop"><img src="images/icono_desktop.jpg" /></a></div>
                    		<div style="width:180px; height:auto; font-size:14px;"><a href="layout.php?id=desktop">Desktop</a></div>
                    	</li>

                    	<li style="width:180px; height:auto; list-style:none; display:inline-block; border:1px solid #666; margin:20px;">
                    		<div style="width:180px; height:180px;"><a href="layout.php?id=mobile"><img src="images/icono_movil.jpg" /></a></div>
                    		<div style="width:180px; height:auto; font-size:14px;"><a href="layout.php?id=mobile">Mobile</a></div>
                    	</li>
                    </ul>
                </div>
                <div id="tab-2">
                    <section class="element-group">
                        <div class="contenedor_elemento">
                            <div class="titulo_elemento">BANNER HEAD</div>
                            <div class="contenedor_info_elemento">
                                <div class="info_elemento"><b>Dimensi&oacute;n:</b> 728x125</div>
                                <div class="pipe_elemento">|</div>
                                <div class="info_elemento"><b>Formato:</b> HTML5 / JPG / GIF</div>
                                <div class="pipe_elemento">|</div>
                                <div class="info_elemento"><b>Peso m&aacute;ximo:</b> 45kb (JPG y GIF) / 100kb (HTML5)</div>
                                <div class="pipe_elemento">|</div>
                                <div class="ver_demo_elemento"><a href="demo.php?id=head">VER DEMO</a></div>
                                <br /><span style="font-style:italic; color:#666; font-size:12px;">Requiere reserva.</span>
                                <br /><span style="font-style:italic; color:#666; font-size:12px;">Se puede implementar por tag o material + tracking de seguimiento.</span>
                            </div>
                        </div>
                        
                        <div class="contenedor_elemento">
                            <div class="titulo_elemento">BANNER HEAD EXPANDIBLE</div>
                            <div class="contenedor_info_elemento">
                                <div class="info_elemento"><b>Dimensi&oacute;n:</b> 728x125 - 728x300</div>
                                <div class="pipe_elemento">|</div>
                                <div class="info_elemento"><b>Observaci&oacute;n:</b> Se deben enviar 2 gr&aacute;ficas, una para el banner expandido y otra para el banner cerrado.</div>
                                <div class="pipe_elemento">|</div>
                                <div class="info_elemento"><b>Formato:</b> JPG / GIF</div>
                                <div class="pipe_elemento">|</div>
                                <div class="info_elemento"><b>Peso m&aacute;ximo:</b> 45kb</div>
                                <div class="pipe_elemento">|</div>
                                <div class="ver_demo_elemento"><a href="demo.php?id=head_expandible">VER DEMO</a></div>
                                <br /><span style="font-style:italic; color:#666; font-size:12px;">Requiere reserva.</span>
                                <br /><span style="font-style:italic; color:#666; font-size:12px;">Se puede implementar por tag o material + tracking de seguimiento.</span>
                            </div>
                        </div>
                    </section>
                    <section class="element-group">
                        <div class="contenedor_elemento">
                            <div class="titulo_elemento">BANNER BRANDING</div>
                            <div class="contenedor_info_elemento">
                                <div class="info_elemento"><b>Dimensi&oacute;n:</b> 300x560</div>
                                <div class="pipe_elemento">|</div>
                                <div class="info_elemento"><b>Formato:</b> HTML5 / JPG / GIF</div>
                                <div class="pipe_elemento">|</div>
                                <div class="info_elemento"><b>Peso m&aacute;ximo:</b> 45kb (JPG y GIF) / 100kb (HTML5)</div>
                                <div class="pipe_elemento">|</div>
                                <div class="ver_demo_elemento"><a href="demo.php?id=branding">VER DEMO</a></div>
                                <br /><span style="font-style:italic; color:#666; font-size:12px;">Requiere reserva.</span>
                                <br /><span style="font-style:italic; color:#666; font-size:12px;">Se puede implementar por tag o material + tracking de seguimiento.</span>
                            </div>
                        </div>
                        <div class="contenedor_elemento">
                            <div class="titulo_elemento">BANNER BRANDING EXPANDIBLE</div>
                            <div class="contenedor_info_elemento">
                                <div class="info_elemento"><b>Dimensi&oacute;n:</b> 300x560 - 560x560</div>
                                <div class="pipe_elemento">|</div>
                                <div class="info_elemento"><b>Observaci&oacute;n:</b> Se deben enviar 2 gr&aacute;ficas, una para el banner expandido y otra para el banner cerrado.</div>
                                <div class="pipe_elemento">|</div>
                                <div class="info_elemento"><b>Formato: </b>JPG / GIF</div>
                                <div class="pipe_elemento">|</div>
                                <div class="info_elemento"><b>Peso m&aacute;ximo:</b> 45kb</div>
                                <!-- <div class="pipe_elemento">|</div>
                                <div class="ver_demo_elemento"><a href="demo.php?id=branding">VER DEMO</a></div> -->
                                <br /><span style="font-style:italic; color:#666; font-size:12px;">Requiere reserva.</span>
                                <br /><span style="font-style:italic; color:#666; font-size:12px;">Se puede implementar por tag o material + tracking de seguimiento.</span>
                            </div>
                        </div>
                        <div class="contenedor_elemento">
                            <div class="titulo_elemento">BANNER BRANDING EXPANDIBLE TRIPLE</div>
                            <div class="contenedor_info_elemento">
                                <div class="info_elemento"><b>Dimensi&oacute;n:</b> 300x560 - 560x560</div>
                                <div class="pipe_elemento">|</div>
                                <div class="info_elemento"><b>Observaci&oacute;n:</b> Se deben enviar 4 gr&aacute;ficas, 3 para el banner expandido y otra para el banner cerrado.</div>
                                <div class="pipe_elemento">|</div>
                                <div class="info_elemento"><b>Formato: </b>JPG / GIF</div>
                                <div class="pipe_elemento">|</div>
                                <div class="info_elemento"><b>Peso m&aacute;ximo:</b> 45kb</div>
                                <!-- <div class="pipe_elemento">|</div>
                                <div class="ver_demo_elemento"><a href="demo.php?id=branding">VER DEMO</a></div> -->
                                <br /><span style="font-style:italic; color:#666; font-size:12px;">Requiere reserva.</span>
                                <br /><span style="font-style:italic; color:#666; font-size:12px;">Se puede implementar por tag o material + tracking de seguimiento.</span>
                            </div>
                        </div>
                    </section>
                    <section class="element-group">    
                        <div class="contenedor_elemento">
                            <div class="titulo_elemento">BANNER CONTENT</div>
                            <div class="contenedor_info_elemento">
                                <div class="info_elemento"><b>Dimensi&oacute;n:</b> 575x250</div>
                                <div class="pipe_elemento">|</div>
                                <div class="info_elemento"><b>Formato:</b> HTML5 / JPG / GIF</div>
                                <div class="pipe_elemento">|</div>
                                <div class="info_elemento"><b>Peso m&aacute;ximo:</b> 45kb (JPG y GIF) / 100kb (HTML5)</div>
                                <div class="pipe_elemento">|</div>
                                <div class="ver_demo_elemento"><a href="demo.php?id=content">VER DEMO</a></div>
                                <br /><span style="font-style:italic; color:#666; font-size:12px;">Requiere reserva.</span>
                                <br /><span style="font-style:italic; color:#666; font-size:12px;">Se puede implementar por tag o material + tracking de seguimiento.</span>
                            </div>
                        </div>
                        <div class="contenedor_elemento">
                            <div class="titulo_elemento">BANNER FOOTER</div>
                            <div class="contenedor_info_elemento">
                                <div class="info_elemento"><b>Dimensi&oacute;n:</b> 1263x62</div>
                                <div class="pipe_elemento">|</div>
                                <div class="info_elemento"><b>Formato:</b> JPG / GIF</div>
                                <div class="pipe_elemento">|</div>
                                <div class="info_elemento"><b>Peso m&aacute;ximo:</b> 45kb</div>
                                <!-- <div class="pipe_elemento">|</div>
                                <div class="ver_demo_elemento"><a href="demo.php?id=content">VER DEMO</a></div> -->
                                <br /><span style="font-style:italic; color:#666; font-size:12px;">Requiere reserva.</span>
                                <br /><span style="font-style:italic; color:#666; font-size:12px;">Se puede implementar por tag o material + tracking de seguimiento.</span>
                            </div>
                        </div>
                        <div class="contenedor_elemento">
                            <div class="titulo_elemento">ITT</div>
                            <div class="contenedor_info_elemento">
                                <div class="info_elemento"><b>Dimensi&oacute;n:</b> 800x600 y 650x125 (Central)</div>
                                <div class="pipe_elemento">|</div>
                                <div class="info_elemento"><b>Formato:</b> HTML5 / JPG / GIF</div>
                                <div class="pipe_elemento">|</div>
                                <div class="info_elemento"><b>Observaci&oacute;n:</b> Se integra autom&aacute;ticamente bot&oacute;n de cierre.</div>
                                <div class="pipe_elemento">|</div>
                                <div class="info_elemento"><b>Restricciones:</b> M&aacute;ximo 10 segundos de duraci&oacute;n.</div>
                                <div class="pipe_elemento">|</div>
                                <div class="ver_demo_elemento"><a href="demo.php?id=itt">VER DEMO</a></div>
                                <br /><span style="font-style:italic; color:#666; font-size:12px;">Requiere reserva.</span>
                                <br /><span style="font-style:italic; color:#666; font-size:12px;">Se debe enviar tracking de seguimiento.</span>
                            </div>
                        </div>

                        <div class="contenedor_elemento">
                            <div class="titulo_elemento">CARRUSEL</div>
                            <div class="contenedor_info_elemento">
                                
                                <div class="info_elemento">
                                    <b>Especificaciones:</b><br /> 
                                    Se deben enviar 2 gr&aacute;ficas de 960x132px, una para el carrusel abierto y otra para el carrusel cerrado.<br />
                                    Se debe enviar el c&oacute;digo de color en formato RGB (Ejemplo:eaf40f) para el fondo del carrusel (Debe ser coherente a la gr&aacute;fica).<br />
                                    Para los &iacute;tems del carrusel (mínimo 5) las medidas son 220x110px.<br />
                                    Las im&aacute;genes miniatura deben venir con descripci&oacute;n del producto inclu&iacute;da. 
                                </div><br />
                                <div class="info_elemento"><b>Peso m&aacute;ximo:</b> 45kb (JPG y GIF) / 100kb (HTML5)</div>
                                <div class="pipe_elemento">|</div>
                                <div class="ver_demo_elemento"><a href="demo.php?id=carrusel">VER DEMO</a></div>
                                <div class="pipe_elemento">|</div>
                                <div class="ver_demo_elemento" onClick="mostrarCarrusel()" style="cursor:pointer; color:blue; text-decoration:underline;">VER EJEMPLO</div>
                                <br /><span style="font-style:italic; color:#666; font-size:12px;">Requiere reserva.</span>
                                <br /><span style="font-style:italic; color:#666; font-size:12px;">Se debe enviar tracking de seguimiento.</span>
                            </div>
                        </div>

                        <div class="contenedor_elemento">
                            <div class="titulo_elemento">BANNER CENTRAL</div>
                            <div class="contenedor_info_elemento">
                                <div class="info_elemento"><b>Dimensi&oacute;n:</b> 650x125</div>
                                <div class="pipe_elemento">|</div>
                                <div class="info_elemento"><b>Formato:</b> HTML5 / JPG / GIF</div>
                                <div class="pipe_elemento">|</div>
                                <div class="info_elemento"><b>Peso m&aacute;ximo:</b> 45kb (JPG y GIF) / 100kb (HTML5)</div>
                                <div class="pipe_elemento">|</div>
                                <div class="ver_demo_elemento"><a href="demo.php?id=central">VER DEMO</a></div>
                                <br /><span style="font-style:italic; color:#666; font-size:12px;">Requiere reserva.</span>
                                <br /><span style="font-style:italic; color:#666; font-size:12px;">Se puede implementar por tag o material + tracking de seguimiento.</span>
                            </div>
                        </div>
                        
                        <div class="contenedor_elemento">
                            <div class="titulo_elemento">BANNER BOX</div>
                            <div class="contenedor_info_elemento">
                                <div class="info_elemento"><b>Dimensi&oacute;n:</b> 300x250</div>
                                <div class="pipe_elemento">|</div>
                                <div class="info_elemento"><b>Formato:</b> HTML5 / JPG / GIF</div>
                                <div class="pipe_elemento">|</div>
                                <div class="info_elemento"><b>Peso m&aacute;ximo:</b> 45kb (JPG y GIF) / 100kb (HTML5)</div>
                                <div class="pipe_elemento">|</div>
                                <div class="ver_demo_elemento"><a href="demo.php?id=box">VER DEMO</a></div>
                                <br /><span style="font-style:italic; color:#666; font-size:12px;">Requiere reserva.</span>
                                <br /><span style="font-style:italic; color:#666; font-size:12px;">Se puede implementar por tag o material + tracking de seguimiento.</span>
                            </div>
                        </div>

                        <div class="contenedor_elemento">
                            <div class="titulo_elemento">PUBLIPOST</div>
                            <div class="contenedor_info_elemento">
                                <div class="info_elemento"><b>Formato:</b> Texto + 2 im&aacute;genes.</div>
                                <div class="pipe_elemento">|</div>
                                <div class="info_elemento"><b>Caracter&iacute;sticas:</b> 3 d&iacute;as destacado en el home (d&iacute;as a elecci&oacute;n)</div>
                                <div class="pipe_elemento">|</div>
                                <div class="ver_demo_elemento"><a href="demo.php?id=publipost">VER DEMO</a></div>
                                <div class="pipe_elemento">|</div>
                                <div class="ver_demo_elemento"><a href="http://www.soychile.cl/Santiago/Publirreportajes/2016/05/17/393736/ENAP-inicia-limpieza-y-anuncia-investigacion-interna-por-fuga-de-producto-en-Quintero.aspx" target="_blank">VER EJEMPLO</a></div>
                                <br /><span style="font-style:italic; color:#666; font-size:12px;">Requiere reserva.</span>
                            </div>
                        </div>

                        <div class="contenedor_elemento">
                            <div class="titulo_elemento">PUBLIPOST + BANNER</div>
                            <div class="contenedor_info_elemento">
                                <div class="info_elemento"><b>Dimensi&oacute;n:</b> 728x125</div>
                                <div class="pipe_elemento">|</div>
                                <div class="info_elemento"><b>Formato:</b> Texto + 2 im&aacute;genes. Banner HTML5 / JPG / GIF</div>
                                <div class="pipe_elemento">|</div>
                                <div class="info_elemento"><b>Caracter&iacute;sticas:</b> 3 d&iacute;as destacado en el home (d&iacute;as a elecci&oacute;n)</div>
                                <div class="pipe_elemento">|</div>
                                <div class="ver_demo_elemento"><a href="demo.php?id=publipost_banner">VER DEMO</a></div>
                                <br /><span style="font-style:italic; color:#666; font-size:12px;">Requiere reserva.</span>
                            </div>
                        </div>

                        <div class="contenedor_elemento">
                            <div class="titulo_elemento">BANNER BOX VIDEO</div>
                            <div class="contenedor_info_elemento">
                                <div class="info_elemento"><b>Dimensi&oacute;n:</b> 300x250</div>
                                <div class="pipe_elemento">|</div>
                                <div class="info_elemento"><b>Formato:</b> Se debe enviar URL del video en Youtube.</div>
                                <div class="pipe_elemento">|</div>
                                <div class="ver_demo_elemento"><a href="demo.php?id=box_video">VER DEMO</a></div>
                                <br /><span style="font-style:italic; color:#666; font-size:12px;">Requiere reserva.</span>
                                <br /><span style="font-style:italic; color:#666; font-size:12px;">Se debe enviar tracking de seguimiento.</span>
                                <br /><span style="font-style:italic; color:#666; font-size:12px;">Tiene auto play sin sonido, el usuario lo puede activar (se replica el reproductor de Youtube).</span>
                            </div>
                        </div>

                        <div class="contenedor_elemento">
                            <div class="titulo_elemento">PREROLL</div>
                            <div class="contenedor_info_elemento">
                                <div class="info_elemento"><b>Formato:</b> Se debe enviar URL del video en Youtube.</div>
                                <div class="pipe_elemento">|</div>
                                <div class="info_elemento"><b>Observaci&oacute;n:</b> Se puede saltar el anuncio a los 5 segundos.</div>
                                <div class="pipe_elemento">|</div>
                                <div class="ver_demo_elemento"><a href="demo.php?id=preroll">VER DEMO</a></div>
                                <br /><span style="font-style:italic; color:#666; font-size:12px;">Requiere reserva.</span>
                                <br /><span style="font-style:italic; color:#666; font-size:12px;">Se debe enviar tracking de seguimiento.</span>
                            </div>
                        </div>
                    </section>
                </div>
            	<div id="tab-3">
            		<div class="contenedor_elemento">
                    	<div class="titulo_elemento">ITT</div>
                        <div class="contenedor_info_elemento">
                        	<div class="info_elemento"><b>Dimensi&oacute;nes:</b> Horizontal 480x220 | Vertical 320x370</div>
                            <div class="pipe_elemento">|</div>
                            <div class="info_elemento"><b>Formato:</b> HTML5 / JPG / GIF</div>
                            <div class="pipe_elemento">|</div>
                            <div class="info_elemento"><b>Peso m&aacute;ximo:</b> 45kb (JPG y GIF) / 100kb (HTML5)</div>
                            <div class="pipe_elemento">|</div>
                            <div class="info_elemento"><b>Observaci&oacute;n:</b> Se integra autom&aacute;ticamente bot&oacute;n de cierre.</div>
                            <div class="pipe_elemento">|</div>
                            <div class="ver_demo_elemento"><a href="demo_mobile.php?id=itt">VER DEMO</a></div>
                            <br /><span style="font-style:italic; color:#666; font-size:12px;">Requiere reserva.</span>
                            <br /><span style="font-style:italic; color:#666; font-size:12px;">Se debe enviar tracking de seguimiento.</span>
                        </div>
                    </div>
            		<div class="contenedor_elemento">
                    	<div class="titulo_elemento">BANNER HEAD / INTERNOTICIA</div>
                        <div class="contenedor_info_elemento">
                        	<div class="info_elemento"><b>Dimensi&oacute;n:</b> 320x100</div>
                            <div class="pipe_elemento">|</div>
                            <div class="info_elemento"><b>Formato:</b> HTML5 / JPG / GIF</div>
                            <div class="pipe_elemento">|</div>
                            <div class="info_elemento"><b>Peso m&aacute;ximo:</b> 45kb (JPG y GIF) / 100kb (HTML5)</div>
                            <div class="pipe_elemento">|</div>
                            <div class="ver_demo_elemento"><a href="demo_mobile.php?id=head">VER DEMO</a></div>
                            <br /><span style="font-style:italic; color:#666; font-size:12px;">Requiere reserva.</span>
                            <br /><span style="font-style:italic; color:#666; font-size:12px;">Se debe enviar tracking de seguimiento.</span>
                        </div>
                    </div>

                    <div class="contenedor_elemento">
                    	<div class="titulo_elemento">BANNER CONTENT</div>
                        <div class="contenedor_info_elemento">
                        	<div class="info_elemento"><b>Dimensi&oacute;n:</b> 300x250</div>
                            <div class="pipe_elemento">|</div>
                            <div class="info_elemento"><b>Formato:</b> HTML5 / JPG / GIF</div>
                            <div class="pipe_elemento">|</div>
                            <div class="info_elemento"><b>Peso m&aacute;ximo:</b> 45kb (JPG y GIF) / 100kb (HTML5)</div>
                            <div class="pipe_elemento">|</div>
                            <div class="ver_demo_elemento"><a href="demo_mobile.php?id=content">VER DEMO</a></div>
                            <br /><span style="font-style:italic; color:#666; font-size:12px;">Requiere reserva.</span>
                            <br /><span style="font-style:italic; color:#666; font-size:12px;">Se debe enviar tracking de seguimiento.</span>
                        </div>
                    </div>

                    <div class="contenedor_elemento">
                            <div class="titulo_elemento">BANNER FOOTER</div>
                            <div class="contenedor_info_elemento">
                                <div class="info_elemento"><b>Dimensi&oacute;n:</b> 320x50</div>
                                <div class="pipe_elemento">|</div>
                                <div class="info_elemento"><b>Formato:</b> JPG / GIF</div>
                                <div class="pipe_elemento">|</div>
                                <div class="info_elemento"><b>Peso m&aacute;ximo:</b> 45kb</div>
                                <!-- <div class="pipe_elemento">|</div>
                                <div class="ver_demo_elemento"><a href="demo.php?id=content">VER DEMO</a></div> -->
                                <br /><span style="font-style:italic; color:#666; font-size:12px;">Requiere reserva.</span>
                                <br /><span style="font-style:italic; color:#666; font-size:12px;">Se puede implementar por tag o material + tracking de seguimiento.</span>
                            </div>
                        </div>
            	</div>

            	<div id="tab-4">
            		<div class="contenedor_elemento">
                    	<div class="titulo_elemento">BIG BANNER IMAGEN + VIDEO</div>
                        <div class="contenedor_info_elemento">
                        	<div class="info_elemento"><b>Dimensi&oacute;nes:</b> 2 Im&aacute;genes de 600x300 + Video (MP4)</div>
                            <div class="pipe_elemento">|</div>
                            <div class="info_elemento"><b>Formato:</b> GIF Animado / JPG / MP4</div>
                            <div class="pipe_elemento">|</div>
                            <div class="info_elemento"><b>Observaci&oacute;n:</b> Se integra autom&aacute;ticamente bot&oacute;n de cierre.</div>
                            <div class="pipe_elemento">|</div>
                            <div class="info_elemento"><b>Peso m&aacute;ximo:</b> 45kb (JPG y GIF)</div>
                            <div class="pipe_elemento">|</div>
                            <div class="ver_demo_elemento"><a href="demo.php?id=bigbanner_video">VER DEMO</a></div>
                            <br /><span style="font-style:italic; color:#666; font-size:12px;">Requiere reserva.</span>
                            <br /><span style="font-style:italic; color:#666; font-size:12px;">Se debe enviar tracking de seguimiento.</span>
                        </div>
                    </div>
                    <div class="contenedor_elemento">
                    	<div class="titulo_elemento">BIG BANNER IMAGEN</div>
                        <div class="contenedor_info_elemento">
                        	<div class="info_elemento"><b>Dimensi&oacute;nes:</b> 600x900</div>
                            <div class="pipe_elemento">|</div>
                            <div class="info_elemento"><b>Formato:</b> JPG / GIF</div>
                            <div class="pipe_elemento">|</div>
                            <div class="info_elemento"><b>Observaci&oacute;n:</b> Se integra autom&aacute;ticamente bot&oacute;n de cierre.</div>
                            <div class="pipe_elemento">|</div>
                            <div class="info_elemento"><b>Peso m&aacute;ximo:</b> 45kb</div>
                            <div class="pipe_elemento">|</div>
                            <div class="ver_demo_elemento"><a href="demo.php?id=bigbanner">VER DEMO</a></div>
                            <br /><span style="font-style:italic; color:#666; font-size:12px;">Requiere reserva.</span>
                            <br /><span style="font-style:italic; color:#666; font-size:12px;">Se debe enviar tracking de seguimiento.</span>
                        </div>
                    </div>
            	</div>
        
            </div>

            
    
		</div><!--cierra contenido-->
    </div><!--cierra fila contenido-->


    <div id="contacto" class="fila_contenido" style="min-height:100%;">
    	<div class="contenido">
    		<div class="contenedor_contacto_movil">
    			<div class="contenedor_item_form" style="border-bottom:1px solid #ccc; padding-bottom:10px; font-size:16px; font-weight:bold;">
    			Completa el formulario y te contactaremos
    			</div>
				<form action="<?php echo $_SERVER['REQUEST_URI']?>" method="post" enctype="multipart/form-data">
					<div class="contenedor_item_form">
						<div class="texto_item_form">Nombre</div>
						<div class="contenedor_campo_item_form"><input name="nombre" type="text" class="campo_item_form"/></div>
					</div>
					<div class="contenedor_item_form">
						<div class="texto_item_form">Correo</div>
						<div class="contenedor_campo_item_form"><input name="mail" type="text" class="campo_item_form"/></div>
					</div>
					<div class="contenedor_item_form">
						<div class="texto_item_form">Tel&eacute;fono</div>
						<div class="contenedor_campo_item_form"><input name="telefono" type="text" class="campo_item_form"/></div>
					</div>
					<div class="contenedor_item_form">
						<div class="texto_item_form">Mensaje</div>
						<div class="contenedor_campo_item_form"><textarea name="comentarios" class="area_item_form"></textarea></div>
					</div>
					<div class="contenedor_item_form">
						<div class="texto_item_form"></div>
						<div class="contenedor_campo_item_form"><input type="submit" value="Enviar" name="boton_enviar" class="boton_alerta"/></div>
					</div>

					<div class="contenedor_item_form" style="border-top:1px solid #ccc; padding-top:10px; font-size:14px;">
						<span style="font-size:18px; font-weight:bold;">Contacto comercial</span><br /><br /><b>Francisca Dur&aacute;n</b><br />francisca.duran@mediosregionales.cl<br /><a href="tel:227534325">22 753 4325</a> | <a href="tel:975393745">97 539 3745</a><br /><br />
						<b>Natalia Navarro</b><br />natalia.navarro@mediosregionales.cl<br /><a href="tel:227534372">22 753 4372</a> / <a href="tel:961932749">96 193 2749</a>
					</div>
				</form>
			</div>
    	</div>
    </div>
    <!-- Responsive Tabs JS -->
    <script src="js/jquery.responsiveTabs.js" type="text/javascript"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            var $tabs = $('#horizontalTab');
            $tabs.responsiveTabs({
                rotate: false,
                startCollapsed: 'accordion',
                collapsible: 'accordion',
                setHash: true,
                //disabled: [3,4],
                activate: function(e, tab) {
                    $('.info').html('Tab <strong>' + tab.id + '</strong> activated!');
                },
                activateState: function(e, state) {
                    //console.log(state);
                    $('.info').html('Switched from <strong>' + state.oldState + '</strong> state to <strong>' + state.newState + '</strong> state!');
                }
            });

            

        });
    </script>

<?php include('footer.php');?>	
